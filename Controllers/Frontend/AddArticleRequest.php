<?php

class Shopware_Controllers_Frontend_AddArticleRequest extends Enlight_Controller_Action
{
    /**
     * @throws Exception
     */
    public function addArticleAction(): void
    {
        $this->Front()->Plugins()->ViewRenderer()->setNoRender();

        $modules     = $this->container->get('modules');
        $basket      = $modules->Basket();
        $request     = $this->Request();
        $baseUrl     = $request->getBaseUrl();
        $orderNumber = trim($request->getParam('sAdd'));
        $quantity    = $request->getParam('sQuantity', 1);
        $voucherCode = $request->getParam('sVoucher');
        $redirectUrl = $request->getParam('sRedirect', $baseUrl . "/checkout/cart");
        $articles    = $modules->Articles();
        $productId   = $articles->sGetArticleIdByOrderNumber($orderNumber);

        if (!empty($productId)) {
            $basket->sAddArticle($orderNumber, $quantity);
        }

        if (isset($voucherCode) && !empty($voucherCode)) {
            if ($redirectUrl === $baseUrl . "/checkout/cart") {
                $basket->sAddVoucher($voucherCode);
            } else {
                $_SESSION['Shopware']['sVoucher'] = $voucherCode;
            }
        }
        $this->redirect($redirectUrl);
    }
}
