<?php

class Shopware_Controllers_Frontend_EmailUtility extends Enlight_Controller_Action
{
    private const CHANNEL_ID = 'cmF0ZW5vd2J1dHRvbg';
    private const RATE_ID    = 'X749E8E42AC81EEF56526DB4B399E1921';

    /**
     * @throws Exception
     */
    public function articleDetailRedirectAction(): void
    {
        $this->Front()->Plugins()->ViewRenderer()->setNoRender();
        $sOrdernumber = $this->Request()->getParam('sOrdernumber');

        $sql = "
            SELECT sa.id FROM s_articles sa 
            LEFT JOIN s_articles_details sad
            ON sa.id = sad.articleID
            WHERE sad.ordernumber = ?
            ";

        $request = Shopware()->Front()->Request();
        if($request === null){
            return;
        }
        $BaseUrl = $request->getBaseUrl();
        $result  = Shopware()->DB()->fetchCol($sql, [$sOrdernumber]);
        $link    = $BaseUrl . '/detail/index/sArticle/' . $result[0];
        $this->redirect($link);
    }

    /**
     * @throws Exception
     */
    public function createTrustedShopRedirectionAction(): void
    {
        $request     = $this->Request();
        $orderNumber = $request->getParam("sOrdNum");

        $sql = '
            SELECT su.email FROM s_order so 
            LEFT JOIN s_user su
            ON so.userID = su.ID
            Where so.ordernumber = ?
            ';

        $result      = Shopware()->DB()->fetchCol($sql, [$orderNumber]);
        $email       = $result[0];
        $email       = urlencode(base64_encode($email));
        $orderNumber = urlencode(base64_encode($orderNumber));
        $params      = [
            'buyerEmail'  => trim($email),
            'shopOrderID' => trim($orderNumber),
            'channel'     => self::CHANNEL_ID,
        ];
        $url         = "https://www.trustedshops.de/bewertung/bewerten_" . self::RATE_ID . ".html";
        $link        = $url . "?" . http_build_query($params);
        $this->redirect($link);
    }
}