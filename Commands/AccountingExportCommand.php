<?php

namespace HrGeneral\Commands;

use HrGeneral\Services\AccountingExportService;
use JsonException;
use Shopware\Commands\ShopwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AccountingExportCommand extends ShopwareCommand
{
    protected function configure(): void
    {
        $this->setName('hr:accounting:export')
             ->setDescription('Exports all orders of last month incl. pdf and mails it to the email/s set in config or parameter')
             ->addOption('force', 'f', InputOption::VALUE_NONE, "Force Export")
             ->addOption('month', 'm', InputOption::VALUE_OPTIONAL, "Set custom export month, requires year as well")
             ->addOption('paymentExport', 'p', InputOption::VALUE_OPTIONAL, "Pass payment name to additionally export order pdf by specific payment")
             ->addOption('recipient', 'r', InputOption::VALUE_OPTIONAL, "Overwrite recipient")
             ->addOption('noMail', 'x', InputOption::VALUE_NONE, "Dont send by mail")
             ->addOption('year', 'y', InputOption::VALUE_OPTIONAL, "Set custom export year, requires month as well")
             ->addOption('from',null , InputOption::VALUE_OPTIONAL, "Set email that the mail get send from")
             ->addOption('fromName', null, InputOption::VALUE_OPTIONAL, "Set Name of email that mail send from")



        ;
    }

    /**
     * @throws JsonException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);

        /** @var AccountingExportService $aes */
        $aes            = $this->container->get('hr_general.services.accounting_export_service');
        $commandOptions = $input->getOptions();
        $options        = [];

        if($commandOptions['noMail']) {
            $options['sendMail'] = !(bool)$commandOptions['noMail'];
        }

        if (!empty($commandOptions['paymentExport'])) {
            $options['paymentName'] = $commandOptions['paymentExport'];
        }

        if (!empty($commandOptions['recipient'])) {
            $options['recipients'] = $commandOptions['recipient'];
        }

        if (!empty($commandOptions['from']) && !empty($commandOptions['fromName'])) {
            $options['from']     = $commandOptions['from'];
            $options['fromName'] = $commandOptions['fromName'];
        }

        if (!empty($commandOptions['year']) && !empty($commandOptions['month'])) {
            $options['date'] = [
                'month' => $commandOptions['month'],
                'year'  => $commandOptions['year'],
            ];
        }

        if($commandOptions['force']) {
            $options['force'] = (bool)$commandOptions['force'];
        }

        $result = $aes->accountingExport($options);

        try {
            $json = json_decode($result, false, 512, JSON_THROW_ON_ERROR);
            $io->{$json->status}($json->message);
        }catch (JsonException $e) {
            $io->newLine(1);
            $io->writeln(trim($result));
            $io->newLine(1);
        }
    }
}
