<?php

namespace HrGeneral\Components\CompilerPass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class AddTemplatePluginDirCompilerPass
 */
class AddTemplatePluginDirCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $template = $container->getDefinition('template');
        $template->addMethodCall('addPluginsDir', [$container->getParameter('hr_general.smarty_dir')]);
    }
}
