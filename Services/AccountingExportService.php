<?php

namespace HrGeneral\Services;

use DateTime;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Exception;
use Zend_Mail;
use Zend_Mail_Exception;
use Zend_Mime;
use Zend_Mime_Part;
use ZipArchive;

class AccountingExportService
{
    private array $attachments;

    private array $config;

    private Connection $connection;

    private string $documentDir;

    private Zend_Mail $mailer;

    private string $month;

    private string $year;

    private array $options;

    private string $passwd;

    private string $downloadDir;

    public function __construct(
        Connection $connection,
        string $documentDir,
        string $downloadDir,
        Zend_Mail $mail,
        $config
    ) {
        $this->connection  = $connection;
        $this->documentDir = $documentDir;
        $this->downloadDir = $downloadDir;
        $this->mailer      = $mail;
        $this->attachments = [];
        $this->config      = $config;

        $this->prepareDate();
    }

    /**
     * @param array $options
     *
     * @return false|string
     */
    public function accountingExport(array $options = [])
    {
        $this->setOptions($options);

        if (!$this->config['exportIsActive'] && !$this->options['force']) {
            return $this->createMessage("warning", "Export is disabled in plugin configs");
        }

        $this->exportInvoices();
        $this->exportCustomers();
        $this->exportOrderPDFs();
        $this->sendMailToAccounting();

        return $this->createMessage("success", "Done");
    }

    /**
     * @param array $options
     */
    private function setOptions(array $options = []): void
    {
        $presets       = [
            "paymentName" => "",
            "sendMail"    => true,
            "recipients"  => $this->getRecipients(),
            "from"        => "info@health-rise.de",
            "fromName"    => "Health Rise Shop",
            "date"        => [],
            "force"       => false,
        ];
        $this->options = array_merge($presets, $options);

        if (!empty($this->options['date'])) {
            $this->month = str_pad($this->options['date']['month'], 2, "0", STR_PAD_LEFT);
            $this->year  = str_pad($this->options['date']['year'], 4, "20", STR_PAD_LEFT);
        }
    }

    private function prepareDate(): void
    {
        [$y, $m] = explode("-", date("Y-m", strtotime("last day of previous month")));

        $this->month = str_pad($m, 2, "0", STR_PAD_LEFT);
        $this->year  = str_pad($y, 4, "20", STR_PAD_LEFT);
    }

    public function isReady(): bool
    {
        $foundFiles = glob($this->documentDir . "[a-z]*_{$this->year}_$this->month.{csv,zip}", GLOB_BRACE);
        $dateTime   = new DateTime('NOW');
        $current    = $dateTime->format('m');
        $dateTime->modify("-1 day");
        $recent = $dateTime->format('m');

        return $current !== $recent || empty($foundFiles) || $this->options['force'];
    }

    private function exportInvoices(): void
    {
        $sql = <<<SQL
            SELECT
                DATE_FORMAT(doc.date, "%Y%m%d") Rechnungsdatum,
                o.ordernumber,
                REPLACE(ROUND(SUM( IF (doc.`type`=4,-d.price,d.price)*d.quantity)+IF(o.invoice_shipping_tax_rate=d.tax_rate,IF(doc.`type`=4,-o.invoice_shipping,o.invoice_shipping),0),2), '.', ',') "Summe Brutto",
                RPAD(u.customernumber,6,"0") Kundenummer,
                TRIM(LEADING "0" FROM doc.docID) Rechnungsnummer,
                IF(d.tax_rate = 19, 84000, 83000) Erloeskonto,
                d.tax_rate Steuer,
                cdoc.name Documententyp,
                GROUP_CONCAT(d.NAME SEPARATOR " | ") Bestelldetails
            FROM s_order o
            JOIN s_order_details d ON d.orderID = o.id
            JOIN s_order_documents doc ON doc.orderID = o.id
            JOIN s_user u ON u.id = o.userID
            JOIN s_core_documents cdoc ON cdoc.id = doc.`type`
            WHERE o.invoice_amount > 0
            AND d.price != 0
            AND DATE_FORMAT(doc.date, "%Y%m") = CONCAT(:year,:month)
            AND o.STATUS IN (0,1,2,3,4,5,6,7,8)
            GROUP BY DATE_FORMAT(o.ordertime, "%Y%m%d"), u.customernumber, doc.docID, doc.id, d.tax_rate
            ORDER BY TRIM(LEADING "0" FROM doc.docID)
        SQL;

        $result = $this->runQuery($sql, ['year' => $this->year, 'month' => $this->month]);

        if (is_array($result) && !empty($result)) {
            $this->storeResults($result, 'orders');
        }
    }

    private function exportCustomers(): void
    {
        $sql = <<<SQL
            SELECT
                RPAD(u.customernumber,6,'0') Kundenummer,
                CONCAT(u.firstname, ' ', u.lastname) AS Name
            FROM s_order o
            JOIN s_order_details d ON d.orderID = o.id
            JOIN s_core_states s ON s.id = o.`status`
            JOIN s_user u ON u.id = o.userID
            JOIN s_order_documents doc ON doc.orderID = o.id
            WHERE DATE_FORMAT(doc.date, "%Y%m") = CONCAT(:year,:month)
            AND o.STATUS BETWEEN 0 AND 8
            AND o.invoice_amount > 0 AND d.price != 0
            GROUP BY u.customernumber, u.firstname, u.lastname
            ORDER BY u.customernumber
        SQL;

        $result = $this->runQuery($sql, ['year' => $this->year, 'month' => $this->month]);
        if (is_array($result) && !empty($result)) {
            $this->storeResults($result, 'customers');
        }
    }

    /**
     * @param string $sql
     * @param array  $params
     *
     * @return array|string
     */
    private function runQuery(string $sql, array $params = [])
    {
        try {
            return $this->connection->executeQuery($sql, $params)->fetchAllAssociative();
        } catch (Exception | \Doctrine\DBAL\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param        $data
     * @param        $name
     * @param string $saveType
     */
    private function storeResults($data, $name, string $saveType = "CSV"): void
    {
        $fn = $name . "_{$this->year}_{$this->month}";

        switch ($saveType) {
            case "CSV":
                $head     = array_flip($data[0]);
                $fileName = $fn . ".csv";
                $filePath = $this->documentDir . $fileName;

                $fp = fopen($filePath, 'wb');
                fputcsv($fp, $head, ";");

                foreach ($data as $row) {
                    $decodedRow = array_map("utf8_decode", $row);
                    fputcsv($fp, $decodedRow, ";");
                }

                fclose($fp);

                $this->attachments[] = [
                    'filename' => $fileName,
                    'mimeType' => 'application/csv',
                ];
                break;

            case "ZIP":
                $zip = new ZipArchive();

                $zipFileName = md5($fn) . ".zip";
                $flag        =
                    file_exists($this->downloadDir . $zipFileName) ? ZipArchive::OVERWRITE : ZipArchive::CREATE;
                if ($zip->open($this->downloadDir . $zipFileName, $flag) === true) {
                    $zip->setPassword($this->getPassword());
                    foreach ($data as $file) {
                        $pdfFile = $this->documentDir . $file['hash'] . ".pdf";
                        if (file_exists($pdfFile)) {
                            if (empty($file['rechnungsnummer'])) {
                                $file['rechnungsnummer'] = $file['hash'];
                            }
                            $zip->addFile($pdfFile, $file['rechnungsnummer'] . ".pdf");
                            $zip->setEncryptionName($file['rechnungsnummer'] . ".pdf", ZipArchive::EM_AES_256
                            );
                        }
                    }

                    $zip->close();
                }

                $this->attachments[] = [
                    'filename' => $zipFileName,
                    'mimeType' => 'application/zip',
                ];
                break;

            default:
                break;
        }
    }

    private function getShopData(): array
    {
        $data = [
            'name' => '',
            'host' => '',
            'baseUrl' => '',
            'secure' => '',
            'fullUrl' => ''
        ];
        try {
            $qb = $this->connection->createQueryBuilder();
            $data = $qb->select(['s.*'])->from('s_core_shops','s')->where('s.id = 1')
                       ->execute()->fetchAssociative()
            ;
        }catch (Exception|\Doctrine\DBAL\Exception $e) {
            return $data;
        }

        return [
            'name' => $data['name'],
            'host' => $data['host'],
            'baseUrl' => $data['base_url'],
            'secure' => $data['secure'],
            'fullUrl' => "http" .($data['secure']?"s":"") . "://" . $data['host'] . (!empty($data['base_url'])?$data['base_url']:"")
        ];
    }

    private function sendMailToAccounting(): void
    {
        $shop = $this->getShopData();
        $this->mailer->clearRecipients();
        $this->mailer->clearSubject();

        $recipients = is_array($this->options['recipients']) ? $this->options['recipients'] : [$this->options['recipients']];

        foreach ($recipients as $key => $recipient) {
            if ($key === array_key_first($recipients)) {
                $this->mailer->addTo($recipient);
            } else {
                $this->mailer->addCc($recipient);
            }
        }

        $bodyAttachments = "";

        foreach ($this->attachments as $attachment) {
            if ($attachment['mimeType'] !== 'application/zip') {
                $this->mailer->addAttachment($this->createAttachment($attachment['filename'], $attachment['mimeType']));
            } else {
                // create download link and attach this to body
                // also care to save zip to download folder
                $bodyAttachments .= $shop['fullUrl'] . "/files/downloads/" . $attachment['filename'] . "<br/>";
            }
        }
        $bodyAttachments .= "Das Passwort für die ZIP-Dateien lautet: <b>" . $this->passwd . "</b><br/><br/>";

        $body =
            "Diese E-Mail beinhaltet die Kunden- und Belegdaten für den Monat <b>$this->month/$this->year</b><br/><br/>";
        $body .= "Es handelt sich hierbei um einen automatisierten Export, welcher immer zum 1. des<br/>";
        $body .= "jeweiligen Folgemonats generiert wird.<br/><br/>";
        $body .= "Bei Rückfragen wenden Sie sich bitte an die IT-Abteilung der Health Rise GmbH.<br/><br/>";
        $body .= $bodyAttachments;
        $body .= "<b>Mit freundlichen Grüßen</b><br/>";
        $body .= "Ihr Health Rise Team<br/>";

        $this->mailer->setBodyHtml($body);

        try {
            $this->mailer->setSubject("Shopware Buchhaltungsexport für $this->month/$this->year");
            $this->mailer->setFrom($this->options['from'], $this->options['fromName']);
            if ($this->options['sendMail']) {
                $this->mailer->send();
            }
        } catch (Zend_Mail_Exception $e) {
            echo $e->getMessage();
        }
    }

    private function getRecipients()
    {
        return explode(";", $this->config['accountingMails']);
    }

    /**
     * @param string $filename
     * @param string $mimeType
     *
     * @return Zend_Mime_Part
     */
    private function createAttachment(string $filename, string $mimeType = 'application/csv'): Zend_Mime_Part
    {
        $filePath                    = $this->documentDir . $filename;
        $content                     = file_get_contents($filePath);
        $zendAttachment              = new Zend_Mime_Part($content);
        $zendAttachment->type        = $mimeType;
        $zendAttachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
        $zendAttachment->encoding    = Zend_Mime::ENCODING_BASE64;
        $zendAttachment->filename    = $filename;

        return $zendAttachment;
    }

    private function exportOrderPDFs(): void
    {
        $zipName = 'pdf_orders';
        $sql     = <<<SQL
            SELECT
                TRIM(LEADING "0" FROM doc.docID) rechnungsnummer,
                doc.hash
            FROM s_order o
            JOIN s_order_details d ON d.orderID = o.id
            JOIN s_order_documents doc ON doc.orderID = o.id
            JOIN s_user u ON u.id = o.userID
            JOIN s_core_documents cdoc ON cdoc.id = doc.`type`
            WHERE o.invoice_amount > 0
            AND d.price != 0
            AND DATE_FORMAT(doc.date, "%Y%m") = CONCAT(LPAD(:year,4,"20"),LPAD(:month,2,"0"))
            AND o.STATUS IN (0,1,2,3,4,5,6,7,8)
            GROUP BY DATE_FORMAT(o.ordertime, "%Y%m%d"), u.customernumber, doc.docID, doc.id, d.tax_rate
        SQL;

        $result = $this->runQuery($sql, ['year' => $this->year, 'month' => $this->month]);

        if (is_array($result) && !empty($result)) {
            $this->storeResults($result, $zipName, "ZIP");
        }

        $paymentName = $this->options['paymentName'];

        if (!empty($paymentName)) {
            $zipName .= '_' . $paymentName;
            $pid     = $this->getPaymentIdByName();
            $sql     = <<<SQL
                SELECT
                    TRIM(LEADING "0" FROM doc.docID) rechnungsnummer,
                    doc.hash
                FROM s_order o
                JOIN s_order_details d ON d.orderID = o.id
                JOIN s_order_documents doc ON doc.orderID = o.id
                JOIN s_user u ON u.id = o.userID
                JOIN s_core_documents cdoc ON cdoc.id = doc.`type`
                WHERE o.invoice_amount > 0
                AND d.price != 0
                AND DATE_FORMAT(doc.date, "%Y%m") = CONCAT(LPAD(:year,4,"20"),LPAD(:month,2,"0"))
                AND o.STATUS IN (0,1,2,3,4,5,6,7,8)
                AND o.paymentID = $pid
                GROUP BY DATE_FORMAT(o.ordertime, "%Y%m%d"), u.customernumber, doc.docID, doc.id, d.tax_rate
            SQL;

            $result = $this->runQuery($sql, ['year' => $this->year, 'month' => $this->month]);

            if (is_array($result) && !empty($result)) {
                $this->storeResults($result, $zipName, "ZIP");
            }
        }
    }

    /**
     *
     * @return false|mixed|array|null
     */
    private function getPaymentIdByName()
    {
        $result = null;
        try {
            $sql    = "SELECT id from s_core_paymentmeans where name = :payment_name";
            $result =
                $this->connection->executeQuery($sql, ['payment_name' => $this->options['paymentName']])->fetchOne();
        } catch (Exception | \Doctrine\DBAL\Exception $e) {
            echo $e->getMessage();
        }

        return $result;
    }

    /**
     * @param $type
     * @param $msg
     *
     * @return false|string
     */
    private function createMessage($type, $msg)
    {
        try {
            return json_encode(
                [
                    "status"  => $type,
                    "message" => $msg,
                ],
                JSON_THROW_ON_ERROR
            );
        } catch (\JsonException $e) {
            return $e->getMessage();
        }
    }

    private function getPassword(): string
    {
        $hash         = md5(microtime(true));
        $this->passwd = $hash;

        return $hash;
    }
}
