<?php

namespace HrGeneral\Services;

use JsonException;

class NewsletterPortal
{
    private array $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param      $email
     * @param bool $isSubscribed
     *
     * @return bool
     */
    public function setNewsletterRegistrationPortal($email, bool $isSubscribed): bool
    {
        try{
            $data = json_encode(
                [
                    'Email'                 => $email,
                    'Language'              => 'de_DE',
                    'IsAccountRegistration' => '0',
                    'IsActive'              => $isSubscribed,
                ],
                JSON_THROW_ON_ERROR
            );
        }catch (JsonException $e) {
            return false;
        }


        $url     = rtrim($this->config['portalApiUrl'], '/') . '/' . "SetNewsletterRegistration";
        $opts    = $this->getOpts('POST', $data);
        $context = stream_context_create($opts);

        return (bool)file_get_contents($url, false, $context);
    }

    /**
     * Gets newsletter subscription status and registration datetime
     *
     * @param $email
     *
     * @return false|mixed
     */
    public function getSubscriptionDataPortal($email)
    {
        if (empty($email)) {
            return false;
        }

        $url     = rtrim($this->config['portalApiUrl'], '/') . '/' . "HasSubscription?email=" . $email;
        $opts    = $this->getOpts();
        $context = stream_context_create($opts);
        $result  = file_get_contents($url, false, $context);

        try {
            return json_decode($result, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            return false;
        }
    }

    /**
     * Returns newsletter subscription status
     *
     * @param $email
     *
     * @return bool
     */
    public function hasSubscriptionPortal($email): bool
    {
        $result = $this->getSubscriptionDataPortal($email);

        if (!$result) {
            return false;
        }

        return $result['data']['hasNewsletter'];
    }

    /**
     * @param string $type
     * @param string $data
     *
     * @return array|array[]
     */
    private function getOpts(string $type = 'GET', string $data = ''): array
    {
        $opts = [
            'http' => [
                'method' => $type,
                'header' => [
                    'Content-type: application/json',
                    'ThirdPartyName: ' . $this->config['portalApiUsername'],
                    'ThirdPartyPassword: ' . $this->config['portalApiPassword'],
                ],
            ],
        ];

        if (!empty($data)) {
            $opts['http']['content'] = $data;
        }

        return $opts;
    }
}
