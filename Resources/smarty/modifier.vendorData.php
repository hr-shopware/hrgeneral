<?php
/**
 * This plugin extends giving data with needed array data
 * @param       $sVendors
 * @param array $renames
 *
 * @return mixed
 */
function smarty_modifier_vendorData($sVendors, $renames = [])
{
    if (!empty($renames)) {
        foreach ($sVendors as $key => $sVendor) {
            foreach ($sVendor as $subKey => $value) {
                if (array_key_exists($subKey, $renames) && !in_array($renames[$subKey], $sVendor, true)) {
                    $sVendors[$key][$renames[$subKey]] = $value;
                }
            }
        }
    }

    return $sVendors;
}
