<?php

use Shopware\Bundle\PluginInstallerBundle\Struct\CategoryStruct;
use Shopware\Models\Article\Article;
use Shopware\Models\Category\Category;

/**
 *
 * @param        $sArticle
 *
 * @param string $separator
 *
 * @param bool   $showMainCategory
 *
 * @return string
 *
 * This was added by me (Dwayne Sharp) This file was found on test server and it is not sure if this is still used..
 * This info was added 29.9.2020 in branch feature/style-vendor-cards
 */
function smarty_modifier_categoryTree(
    $sArticle,
    string $separator = '<span class="hr--cat-separator"></span>',
    bool $showMainCategory = true
): ?string {
    $categoryString = null;
    $id             = $sArticle['articleID'];
    if (($categoryTree = getCategoryByArticleId($id, $showMainCategory)) !== null) {
        $categoryString = $categoryTree;
        if (strpos($categoryTree, '|') !== false) {
            $categories     = explode('|', $categoryTree);
            $categoryString = implode(' ' . $separator . ' ', $categories);
        }
    }

    return $categoryString;
}

/**
 * @param $id
 * @param $showMainCategory
 *
 * @return string
 */
function getCategoryByArticleId($id, $showMainCategory): string
{
    $tree         = '';
    $container    = Shopware()->Container();
    $modelManager = $container->get('models');

    /** @var Article $articleModel */
    $articleModel = $modelManager->getRepository(Article::class)->find($id);
    /** @var CategoryStruct $lastCategory */
    $lastCategory = $articleModel->getCategories()->last();
    $cId          = $lastCategory->getParentId();
    /** @var CategoryStruct $category */
    $categoryRepo = $modelManager->getRepository(Category::class);
    $category     = $categoryRepo->find($cId);

    if ($category === null) {
        return '';
    }

    if ($showMainCategory) {
        $parentCategoryId = $category->getParentId();
        /** @var CategoryStruct $mainCategory */
        $mainCategory = $categoryRepo->find($parentCategoryId);
        $tree         = $mainCategory->getName() . '|';
    }

    return $tree . $category->getName();
}
