<?php

function smarty_modifier_boldHandler($content)
{
    return preg_replace('/<(b|strong)>(.*?)<\/(b|strong)>/', '<span class="is--strong">$2 </span>', $content);
}
