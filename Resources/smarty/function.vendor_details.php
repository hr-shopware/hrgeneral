<?php
/**
 * Smarty plugin
 *
 * This plugin is only for Smarty2 BC
 *
 * @package    Smarty
 * @subpackage PluginsFunction
 */

use Psr\Container\ContainerInterface;

/**
 * Smarty {vendor_details} function plugin
 *
 * Type:     function<br>
 * Name:     vendor_details<br>
 * Purpose:  retrieve details of vendor in template
 *
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 *
 * @return null
 * @author   Dwayne Sharp
 *
 * This was changed by me since the service seems to be not present anymore. Obviously this was in an other branch.
 * Since this is currently not in use, its still in here for legacy reasons. Will be removed soon!
 * This info was added 29.9.2020 in branch feature/style-vendor-cards
 */
function smarty_function_vendor_details($params, $template)
{
    $requiredParameters   = ['assign', 'id'];
    try{
        $container            = Shopware()->Container();
        $vendorDetailsService = $container->get('hr_vendor_details.vendor_details_service');
    }catch (Exception $e){
        return null;
    }


    foreach ($requiredParameters as $parameter) {
        if (empty($params[$parameter])) {
            trigger_error("vendor_details: parameter '$parameter' is empty", E_USER_WARNING);

            return;
        }
    }

    $details = $vendorDetailsService->getVendorDetails($params['id']);
    setBaseVariables($details);
    getDistances($container, $details);
    getOpenUntil($details);

    $template->assign($params['assign'], $details);

    return null;
}

function getOpenUntil(&$details)
{
    $openingTimesJson = $details['branches']['mainBranch']['opening_times'];
    if (empty($openingTimesJson)) {
        return;
    }

    $details['branches']['mainBranch']['has']['opening_times'] = true;

    $openingTimesArray    = json_decode($openingTimesJson, true);
    $sCurrentDayOfTheWeek = strtolower(date('l'));
    $openingTime          = end($openingTimesArray['times'][$sCurrentDayOfTheWeek])['from'];
    $endTime              = end($openingTimesArray['times'][$sCurrentDayOfTheWeek])['to'];
    $currentTime          = date('H:i');

    $details['branches']['mainBranch']['close_time'] = $endTime;
    if ($currentTime < $openingTime || $currentTime > $endTime) {
        $details['branches']['mainBranch']['is']['closed'] = true;
    }
}

function getDistances(ContainerInterface $container, &$details)
{
    if (isset($_GET['hr_address_filter'])) {
        $geoService = $container->get('hr_map_article_search.geo_service');
        $geoService->setUserLocation($_GET['hr_address_filter']);
        $geoService->setRadius($_GET['hr_radius_filter']);

        // add distance to main branch
        $lat = $details['branches']['mainBranch']['latitude'];
        $lon = $details['branches']['mainBranch']['longitude'];

        if ($geoService->distanceInRange($lat, $lon)) {
            $details['branches']['mainBranch']['distance']        = $geoService->getDistance();
            $details['branches']['mainBranch']['has']['distance'] = true;
        }
        //foreach subBranch set distance
    }
}

function setBaseVariables(&$details)
{
    //has
    $details['branches']['mainBranch']['has']['distance']      = false;
    $details['branches']['mainBranch']['has']['opening_times'] = false;

    //is
    $details['branches']['mainBranch']['is']['closed'] = false;
}

?>
