<?php
/*

<div class="opening--time-title hr--title-three">{s name='OpeningTime'}Öffnungszeiten{/s}</div>
{if {$SupplierAttributes.opening_time}}
{foreach $SupplierAttributes.opening_time as $item}
{if {$item.BranchId} eq {$branch.id}}
<div class="opening-time">
    <span class="opening--time-day">
        {$dayNames[$item.Day]}:
    </span>
    {$item.From}-{$item.To}
</div>
{/if}
{/foreach}
{/if}
*/

/**
 * This plugin extends giving data with needed array data
 *
 * @param      $opening_time_json
 * @param int  $branchId
 *
 * @return mixed
 */
function smarty_modifier_otFormat($opening_time_json, $branchId = 0): string
{
    $template =
        "<div class='opening-time'><span class='opening-time--day'>{{DAY}}</span><span class='opening-time--time'>{{FROM}} - {{TO}}</span></div>";
    $days     = ['', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag'];

    $ot = json_decode(strip_tags($opening_time_json), true);

    if (empty($ot)) {
        return '';
    }

    $dayTimes = [];
    $searches = ["{{DAY}}", "{{FROM}}", "{{TO}}"];

    foreach ($ot as $entry) {
        $bid              = $entry['BranchId'] ?: 0;
        $dayTimes[$bid][] = str_replace($searches, [$days[$entry['Day']], $entry['From'], $entry['To']], $template);
    }

    if (!isset($dayTimes[$branchId])) {
        return '';
    }

    return implode("", $dayTimes[$branchId]);
}
