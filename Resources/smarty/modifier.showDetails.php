<?php
/*

<div class="opening--time-title hr--title-three">{s name='OpeningTime'}Öffnungszeiten{/s}</div>
{if {$SupplierAttributes.opening_time}}
{foreach $SupplierAttributes.opening_time as $item}
{if {$item.BranchId} eq {$branch.id}}
<div class="opening-time">
    <span class="opening--time-day">
        {$dayNames[$item.Day]}:
    </span>
    {$item.From}-{$item.To}
</div>
{/if}
{/foreach}
{/if}
*/

/**
 * This plugin extends giving data with needed array data
 *
 * @param       $supplier
 * @param array $checks
 *
 * @return bool
 */
function smarty_modifier_showDetails($supplier, $checks = []): bool
{
    $amountOfDetailsFound = 0;
    foreach ($checks as $check){

        switch ($check){
            case 'ot':
                $ot = json_decode(strip_tags($supplier['opening_time']), true);
                if(!empty($ot)){
                    $amountOfDetailsFound++;
                }
                break;
            case 'addr':
                $address_partials = ['addressline1', 'housenumber', 'city', 'zipcode'];
                $addr = $supplier['mainBranch'];
                $addressArray = [];
                if(!empty($addr)){
                    foreach ($address_partials as $partial){
                        $addressArray[$partial] = $addr[$partial];
                    }

                    if(count($address_partials) === count(array_filter($addressArray))){
                        $amountOfDetailsFound++;
                    }
                }
                break;
            case 'subAddr':
                if(isset($supplier['subBranches']) && !empty($supplier['subBranches'])){
                    $amountOfDetailsFound++;
                }
                break;
            default:
                break;
        }
    }

    return $amountOfDetailsFound>0;
}
