<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     modifier.sortby.php
 * Type:     function
 * Name:     sortby
 * Purpose:  sort the content and remove 
 * -------------------------------------------------------------
 */

function array_sort_by_column(&$arr, $col, $dir = SORT_ASC)
{
    $sort_col = [];
    foreach ($arr as $key => $row) {
        $sort_col[$key] = $row[$col];
    }

    array_multisort($sort_col, $dir, $arr);
}

function array_make_it_unique($array, $uniqueElement, $resolution)
{
    $uniqueElementList = [];
    $out = [];

    foreach ($array as $key => $val) {
        if (!in_array($val[$uniqueElement], $uniqueElementList)) {
            if (strlen($val["supplierImg"]) > 0) {
                $file     = new SplFileInfo($val["supplierImg"]);
                $ext      = '.' . $file->getExtension();
                $filename = $file->getBasename($ext) . $resolution . $ext;
                /* set the image path ready for media service */
                $val["supplierImgForMediaService"] = "/media/image/thumbnails/" . $filename;
            }
            array_push($uniqueElementList, $val[$uniqueElement]);
            array_push($out, $val);
        }
    }

    return $out;
}

# 
# Modifier: sortby - allows arrays of named arrays to be sorted by a given field 
# 
function smarty_modifier_sortby($arrData, $sortfields, $uniqueElement = 'OptionalParameter', $resolution = "_200x200")
{
    array_sort_by_column($arrData, $sortfields);

    /* remove double elements if needed */
    if ($uniqueElement != "OptionalParameter") {
        $arrData = array_make_it_unique($arrData, $uniqueElement, $resolution);
    }

    return $arrData;
}

?>