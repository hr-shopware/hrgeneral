<?php

function smarty_modifier_shortcuts($content)
{
    $container = Shopware()->Container();
    $cReader   = $container->get('shopware.plugin.config_reader');

    if ($cReader === null) {
        return $content;
    }

    $configs     = $cReader->getByPluginName('hrGeneral');
    $shortcutMap = [];

    if (isset($configs['titleShortcutList']) && !empty($configs['titleShortcutList'])) {
        $parts = explode(";", $configs['titleShortcutList']);
        foreach ($parts as $part) {
            if (!empty($part)) {
                $titleShortcut                        = explode(":", $part);
                $shortcutMap[trim($titleShortcut[0])] = trim($titleShortcut[1]);
            }
        }
    }

    $sc = array_search($content, $shortcutMap, true);

    return $sc ?: $content;
}
