<?php

/**
 * @param $supplier
 *
 * @return mixed
 */
function smarty_modifier_supplierBranches($supplier)
{
    try {
        $supplierService = Shopware()->Container()->get('hr_vendor_category_listing.service.supplier');
    }catch (Exception $e){
        return $supplier;
    }
    $supplier['mainBranch'] = $supplierService->getSupplierMainBranch($supplier['id']);
    $supplier['subBranches'] = $supplierService->getSupplierBranches($supplier['id']);

    return $supplier;
}
