<?PHP
// Original PHP code by Chirp Internet: www.chirp.com.au
// Please acknowledge use of this code by including this header.

function restoreTags($input)
{
    $opened = [];

    // loop through opened and closed tags in order
    if (preg_match_all("/<(\/?[a-z]+)>?/i", $input, $matches)) {
        foreach ($matches[1] as $tag) {
            if (preg_match("/^[a-z]+$/i", $tag, $regs)) {
                // a tag has been opened
                if (strtolower($regs[0]) != 'br') {
                    $opened[] = $regs[0];
                }
            } elseif (preg_match("/^\/([a-z]+)$/i", $tag, $regs)) {
                // a tag has been closed
                unset($opened[array_pop(array_keys($opened, $regs[1]))]);
            }
        }
    }

    // close tags that are still open
    if ($opened) {
        $tagsToClose = array_reverse($opened);
        foreach ($tagsToClose as $tag) {
            $input .= "</$tag>";
        }
    }

    return $input;
}

/**
 * Smarty plugin
 *
 * @package Smarty
 * @subpackage PluginsModifier
 */

/**
 * Smarty truncate and restore tags modifier plugin
 *
 * Type:     modifier<br>
 * Name:     truncate<br>
 * Purpose:  Truncate a string to a certain length if necessary and restore opened html tags,
 *               optionally splitting in the middle of a word, and
 *               appending the $etc string or inserting $etc into the middle.
 *
 * @link http://smarty.php.net/manual/en/language.modifier.truncate.php truncate (Smarty online manual)
 * @author Dwayne Sharp
 * @param string  $string      input string
 * @param integer $length      length of truncated text
 * @param string  $etc         end string
 * @param boolean $break_words truncate at word boundary
 * @param boolean $middle      truncate in the middle of text
 * @return string truncated string
 */
function smarty_modifier_truncate_restore_tags($string, $length = 80, $etc = '...', $break_words = false, $middle = false) {
    if ($length == 0)
        return '';

    if (Smarty::$_MBSTRING) {
        if (mb_strlen($string, Smarty::$_CHARSET) > $length) {
            $length -= min($length, mb_strlen($etc, Smarty::$_CHARSET));
            if (!$break_words && !$middle) {
                $string = preg_replace('/\s+?(\S+)?$/' . Smarty::$_UTF8_MODIFIER, '', mb_substr($string, 0, $length + 1, Smarty::$_CHARSET));
            }
            if (!$middle) {
                return restoreTags(mb_substr($string, 0, $length, Smarty::$_CHARSET) . $etc);
            }
            return restoreTags(mb_substr($string, 0, $length / 2, Smarty::$_CHARSET) . $etc . mb_substr($string, - $length / 2, $length, Smarty::$_CHARSET));
        }
        return restoreTags($string);
    }

    // no MBString fallback
    if (isset($string[$length])) {
        $length -= min($length, strlen($etc));
        if (!$break_words && !$middle) {
            $string = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $length + 1));
        }
        if (!$middle) {
            return restoreTags(substr($string, 0, $length) . $etc);
        }
        return restoreTags(substr($string, 0, $length / 2) . $etc . substr($string, - $length / 2));
    }
    return restoreTags($string);
}

?>
