<?php
/**
 * Smarty plugin
 *
 * This plugin is only for Smarty2 BC
 *
 * @package    Smarty
 * @subpackage PluginsFunction
 */

use Psr\Container\ContainerInterface;

function smarty_function_getSupplier($params, Enlight_Template_Default $template): bool
{
    $requiredParameters = ['assign'];
    if (!array_keys_exists($requiredParameters, $params)) {
        return false;
    }

    try {
        $container              = Shopware()->Container();
        $geoData                = geoHandler($params);
        $supplierService        = $container->get('hr_vendor_category_listing.service.supplier');
        $categoryService        = $container->get('hr_vendor_category_listing.service.category');
        $sSearchRequest         = $params['sSearchRequest'];
        $hrVendorListingConfigs = $params['hrVendorListingConfigs'];

        $mainCategoryId = $hrVendorListingConfigs['main_category_id'];
        $depth          = $params['depth'] ?: $hrVendorListingConfigs['depth'];
        $limit          = $params['limit'] ?: $hrVendorListingConfigs['totalLimit'];
        $offset         = $params['offset'] ?: 0;
        $sSearch        = $sSearchRequest['sSearch'];
    } catch (Exception $e) {
        return false;
    }

    $supplierCategoryIds = $categoryService->getChildCategoryIds(
        $mainCategoryId,
        $depth
    );

    $supplier = $supplierService->getSupplier(
        $supplierCategoryIds,
        false,
        $offset,
        $limit,
        $sSearch,
        $geoData,
        'Smarty-Plugin: ' . $params['from']
    );

    $template->assign($params['assign'], $supplier['data']);
    $template->assign('totalSupplierFound', $supplier['count']);

    return false;
}

function geoHandler($params): array
{
    if (isset($params['url'])) {
        $parts = parse_url($params['url']);
        parse_str($parts['query'], $queryArray);

        return $queryArray;
    }

    return [];
}

function array_keys_exists($required, $haystack): bool
{
    return count(array_merge(array_flip($required), $haystack)) === count($haystack);
}

?>
