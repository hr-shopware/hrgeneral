<?php
/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * @param string $salutation
 *
 * @return mixed|string|null
 */

function smarty_modifier_hrSalutation(string $salutation)
{
    $snippets = Shopware()->Container()->get('snippets');
    $label = $snippets->getNamespace('frontend/health_rise/salutation')->get($salutation);
    if (trim($label) === '') {
        return $salutation;
    }
    return $label;
}
