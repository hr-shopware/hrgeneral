<?php

namespace Shopware\Themes\Minimal_Design;

use Shopware\Components\Form as Form;
use Shopware\Components\Theme\ConfigSet;

class Theme extends \Shopware\Components\Theme
{
    protected $extend = 'Responsive';

    protected $name = <<<'SHOPWARE_EOD'
Minimales Design
SHOPWARE_EOD;

    protected $description = <<<'SHOPWARE_EOD'
Health Rise GmbH Minimal Design
SHOPWARE_EOD;

    protected $author = <<<'SHOPWARE_EOD'
Dwayne Sharp
SHOPWARE_EOD;

    protected $license = <<<'SHOPWARE_EOD'
proprietary
SHOPWARE_EOD;

    /**
     * Holds default theme colors.
     *
     * @var array
     */
    private $themeColorDefaults = [
        'brand-primary' => '#A4CA4D',
        'brand-secondary' => '#23405C',
        'brand-secondary-dark' => '#23405C'
    ];

    public function createConfig(Form\Container\TabContainer $container)
    {
    }
}
