{extends file="parent:documents/index.tpl"}

{block name="document_index_css"}
    {$smarty.block.parent}
    .santander-table {
        background: #EFEFEF;
        padding: 0 10px;
    }

    .santander-table tr:nth-child(1) td {
        line-height: 12px;
    }

    .santander-table tr:nth-child(1n+2) td {
        line-height: 1px;
    }


{/block}

{block name="document_index_address_base"}
    {if $User.$address.company}{$User.$address.company}<br />{/if}
    {$User.$address.salutation|hrSalutation}
    {if {config name="displayprofiletitle"}}
        {$User.$address.title}<br/>
    {/if}
    {$User.$address.firstname} {$User.$address.lastname}<br />
    {$User.$address.street}<br />
{/block}

{block name="document_index_info_net"}
    {if $Document.netto == true}
        <p>{s name="DocumentIndexAdviceNet"}{/s}</p>
    {/if}

    <p>{s name="DocumentIndexSelectedPayment"}{/s} {$Order._payment.description}</p>
    <table class="santander-table">
        {foreach from=$Order._order.attributes item=attribute name="payeverAttrLoop" key=attrKey}

            {if $attrKey == 'payever_payment_details'}
                {assign var="pe_details" value=$attribute|@json_decode:true}
                {if $pe_details && $pe_details.usage_text}

                    <tr>
                        <td colspan="3">Sie haben bei dieser Bestellung eine Zahlungsart der Santander gewählt.<br>
                            In Kürze erhalten Sie von der Santander eine Email mit allen Informationen
                            zu den weiteren Schritten und Zahlungsfristen.<br>
                            Bitte achten Sie darauf, das Geld nicht an uns, sondern an das unten angegebene Konto der Santander zu
                            überweisen und dabei<br>
                            den untenstehenden Verwendungszweck anzugeben.</td>
                    </tr>
                    <tr><td ><strong>Empfänger</strong>:</td><td >Santander Consumer Bank AG</td></tr>
                    <tr><td><strong>IBAN</strong>:</td><td>DE89 3101 0833 8810 0761 20</td></tr>
                    <tr><td><strong>BIC</strong>:</td><td>SCFBDE33XXX</td></tr>
                    <tr><td><strong>Verwendungszweck</strong>:</td><td>{$pe_details.usage_text} <i>(BITTE IMMER MIT ANGEBEN)</i></td></tr>

                {/if}
                {break}
            {/if}
        {/foreach}
    </table>
{/block}
