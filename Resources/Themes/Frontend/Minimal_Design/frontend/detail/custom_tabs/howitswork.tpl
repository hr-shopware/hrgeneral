<div class="tab--container product-details hr--supplier-tab-container">

    {* Title *}
    <div class="tab--header">
        <a href="#" class="tab--title" title="vendor">
             {s namespace="frontend/health_rise/details/tabs" name="detail_tab_howto"}{/s}
        </a>
    </div>

    {* Preview *}
    <div class="tab--preview">
        {$sArticle.attr1|strip_tags|truncate:45:'...'}<a href="#" class="tab--link" title="mehr">  {s namespace="frontend/health_rise/details/tabs" name="detail_tab_more"}{/s}</a>
    </div>

    {* Content *}
    <div class="tab--content content--product-reviews">
        <div class="buttons--off-canvas">
            <a href="#" title="{"{s name="OffcanvasCloseMenu" namespace="frontend/detail/description"}{/s}"|escape}" class="close--off-canvas">
                <i class="icon--arrow-left"></i>
                {s name="OffcanvasCloseMenu" namespace="frontend/detail/description"}{/s}
            </a>
        </div>

        <div class="hr--info-content">
            {* How its work *}
            {if $sArticle.attr1}
                <li class="base-info--entry entry-attribute">
                    <span class="entry--content hr--custom-description">
                        {$sArticle.attr1|regex_replace:"/&lt;[^&g]*&gt;/":""|nl2br|boldHandler}
                    </span>
                </li>
            {/if}
        </div>
    </div>
</div>
