<div class="content_container item {$class}">
    <div class="content_title">
        <h2>{$title}</h2>
    </div>
    <div class="content_data{if !empty($content|trim)} custom-content{/if}">
        {$content}
        {if !empty($tpl)}
            {include file="frontend/detail/$tpl"}
        {/if}
    </div>
</div>