{extends file="parent:frontend/detail/content/buy_container.tpl"}

{block name="frontend_detail_rich_snippets_brand"}
    <div itemprop="brand" itemtype="https://schema.org/Brand" itemscope>
        <meta itemprop="name" content="{$sArticle.supplierName|escape}" />
    </div>
{/block}

{block name='frontend_detail_index_buy_container_inner'}
    {$smarty.block.parent}
    {include file="frontend/detail/content/data_block.tpl" content=$sArticle.description}
    <br />
    <br />
{/block}

{* Product attribute 1 - freitextfeld 1 *}
{block name='frontend_detail_data_attributes_attr1'}{/block}

{* Product attribute 2 - freitextfeld 2 *}
{block name='frontend_detail_data_attributes_attr2'}{/block}


{* Product SKU *}
{block name="frontend_detail_data_ordernumber"}
	<meta itemprop="productID" content="{$sArticle.articleDetailsID}"/>
	<meta itemprop="sku" content="{$sArticle.ordernumber}"/>
    {*{if !{config name=VoteDisable}}
        <div class="is--hidden">
            <a href="#product--publish-comment" class="product--rating-link" rel="nofollow" title="{$snippetDetailLinkReview|escape}">
                {include file='frontend/_includes/rating.tpl' points=$sArticle.sVoteAverage.average type="aggregated" count=$sArticle.sVoteAverage.count}
            </a>
        </div>
    {/if}*}
{/block}
