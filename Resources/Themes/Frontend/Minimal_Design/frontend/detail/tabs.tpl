{extends file="parent:frontend/detail/tabs.tpl"}

{block name="frontend_detail_tabs_navigation_inner"}

    {* Description tab *}
    {block name="frontend_detail_tabs_description"}
        <a href="#" class="tab--link" title="{s namespace="frontend/detail/tabs" name='DetailTabsDescription'}{/s}"
           data-tabName="description">
            {s namespace="frontend/detail/tabs" name="DetailTabsDescription"}Beschreibung{/s}
        </a>
    {/block}

    {* Produktdetails: tab *}
    {block name="frontend_detail_tabs_nav_supplier_sogeht"}
        <a href="#" class="tab--link" title="{*s name='detail/tabs/supplier_info'}{/s*}"
           data-tabName="productdetails">
            {s namespace="frontend/health_rise/details/tabs" name="detail_tab_productdetails"}{/s}
        </a>
    {/block}

    {* Rating tab *}
    {block name="frontend_detail_tabs_rating"}
        {if !{config name=VoteDisable}}
            <a href="#" class="tab--link" title="{s namespace="frontend/detail/tabs" name='DetailTabsRating'}{/s}"
               data-tabName="rating">
                {s namespace="frontend/detail/tabs" name="DetailTabsRating"}Bewertungen{/s}
                {block name="frontend_detail_tabs_navigation_rating_count"}
                    <span class="product--rating-count">{$sArticle.sVoteAverage.count}</span>
                {/block}
            </a>
        {/if}
    {/block}

    {* So geht's: tab *}
    {block name="frontend_detail_tabs_nav_supplier_sogeht"}
        <a href="#" class="tab--link" title="{*s name='detail/tabs/supplier_info'}{/s*}"
           data-tabName="howto">
            {s namespace="frontend/health_rise/details/tabs" name="detail_tab_howto"}{/s}
        </a>
    {/block}

    {* Supplier Info tab *}
    {block name="frontend_detail_tabs_nav_supplier_info"}
        <a href="#" class="tab--link" title="{*s name='detail/tabs/supplier_info'}{/s*}"
           data-tabName="supplier_info">
            {s namespace="frontend/health_rise/details/tabs" name="detail_tab_supplier_info"}{/s}
        </a>
    {/block}

{/block}

{block name="frontend_detail_tabs_content_inner"}

    {* Description container *}
    {block name="frontend_detail_tabs_content_description"}
        <div class="tab--container">
            {block name="frontend_detail_tabs_content_description_inner"}
                {* Description title *}
                {block name="frontend_detail_tabs_content_description_title"}
                    <div class="tab--header">
                        {block name="frontend_detail_tabs_content_description_title_inner"}
                            <a href="#" class="tab--title"
                               title="{s namespace="frontend/detail/tabs" name='DetailTabsDescription'}{/s}">{s name='DetailTabsDescription'}{/s}</a>
                        {/block}
                    </div>
                {/block}

                {* Description preview *}
                {block name="frontend_detail_tabs_description_preview"}
                    <div class="tab--preview">
                        {block name="frontend_detail_tabs_content_description_preview_inner"}
                            {$sArticle.description_long|strip_tags|truncate:100:'...'}<a href="#" class="tab--link"
                                                                                         title="{s namespace="frontend/detail/tabs" name="PreviewTextMore"}{/s}">{s namespace="frontend/detail/tabs" name="PreviewTextMore"}{/s}</a>
                        {/block}
                    </div>
                {/block}

                {* Description content *}
                {block name="frontend_detail_tabs_content_description_description"}
                    <div class="tab--content">
                        {block name="frontend_detail_tabs_content_description_description_inner"}
                            {include file="frontend/detail/tabs/description.tpl"}
                        {/block}
                    </div>
                {/block}
            {/block}
        </div>
    {/block}

    {* Produktdetails tab *}
    {include file="frontend/detail/custom_tabs/productdetails.tpl"}

    {* Rating container *}
    {block name="frontend_detail_tabs_content_rating"}

        {if !{config name=VoteDisable}}
            <div class="tab--container">
                {block name="frontend_detail_tabs_content_rating_inner"}

                    {* Rating title *}
                    {block name="frontend_detail_tabs_rating_title"}
                        <div class="tab--header">
                            {block name="frontend_detail_tabs_rating_title_inner"}
                                <a href="#" class="tab--title"
                                   title="{s name='DetailTabsRating'}{/s}">{s name='DetailTabsRating'}{/s}</a>
                                {block name="frontend_detail_tabs_rating_title_count"}
                                    <span class="product--rating-count">{$sArticle.sVoteAverage.count}</span>
                                {/block}
                            {/block}
                        </div>
                    {/block}

                    {* Rating preview *}
                    {block name="frontend_detail_tabs_rating_preview"}
                        <div class="tab--preview">
                            {block name="frontend_detail_tabs_rating_preview_inner"}
                                {s name="RatingPreviewText"}{/s}<a href="#" class="tab--link"
                                                                   title="{s name="PreviewTextMore"}{/s}">{s name="PreviewTextMore"}{/s}</a>
                            {/block}
                        </div>
                    {/block}

                    {* Rating content *}
                    {block name="frontend_detail_tabs_rating_content"}
                        <div id="tab--product-comment" class="tab--content">
                            {block name="frontend_detail_tabs_rating_content_inner"}
                                {include file="frontend/detail/tabs/comment.tpl"}
                            {/block}
                        </div>
                    {/block}

                {/block}
            </div>
        {/if}
    {/block}

    {* So gehts tab *}
    {include file="frontend/detail/custom_tabs/howitswork.tpl"}

    {* Vendor details tab *}
    {*include file="frontend/custom_tabs/vendortab.tpl"*}

{/block}



{block name="frontend_detail_tabs_content_description_inner"}

        <div class="description--container">
            {$smarty.block.parent}
        </div>

{/block}
