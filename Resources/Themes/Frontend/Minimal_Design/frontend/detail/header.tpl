{extends file="parent:frontend/detail/header.tpl"}

{**}
{block name='frontend_index_header_meta_robots'}all{/block}

{* Description *}
{block name="frontend_index_header_meta_description"}
    {if $sArticle.attr8} {$sArticle.attr8|regex_replace:"/[\r\n]/":" "|truncate:120:"...":false}{else}{$sArticle.description|truncate:120:"...":false|escape}{/if}
{/block}

{*Title*}
{block name='frontend_index_header_title'}{strip}
    {if $sArticle.attr9}{$sArticle.attr9|truncate:55:"...":false|escapeHtml}{/if}
{/strip}{/block}

{block name="frontend_index_header_title"}{if $sArticle.metaTitle}{$sArticle.metaTitle|escapeHtml} | {{config name="sShopname"}|escapeHtml}{else}{$sArticle.articleName|escape} | {$smarty.block.parent}{/if}{/block}


{* Meta opengraph tags *}
{block name='frontend_index_header_meta_tags_opengraph'}
    <meta property="og:type" content="product" />
    <meta property="og:site_name" content="{{config name=sShopname}|escapeHtml}" />
    <meta property="og:url" content="{url sArticle=$sArticle.articleID title=$sArticle.articleName}" />
    <meta property="og:title" content="{if $sArticle.attr9}{$sArticle.attr9|escapeHtml}{else}{$sArticle.articleName|escapeHtml}{/if}" />
    <meta property="og:description" content="{if $sArticle.attr8} {$sArticle.attr8|regex_replace:"/[\r\n]/":" "}{else}{$sArticle.description_long|strip_tags|trim|truncate:240|escapeHtml}{/if}" />
    <meta property="og:image" content="{$sArticle.image.source}" />

    <meta property="product:brand" content="{$sArticle.supplierName|escapeHtml}" />
    <meta property="product:price" content="{$sArticle.price}" />
    <meta property="product:product_link" content="{url sArticle=$sArticle.articleID title=$sArticle.articleName}" />

    <meta name="twitter:card" content="product" />
    <meta name="twitter:site" content="{{config name=sShopname}|escapeHtml}" />
    <meta name="twitter:title" content="{if $sArticle.attr9}{$sArticle.attr9|escapeHtml}{else}{$sArticle.articleName|escapeHtml}{/if}" />
    <meta name="twitter:description" content="{if $sArticle.attr8} {$sArticle.attr8|regex_replace:"/[\r\n]/":" "}{else}{$sArticle.description_long|strip_tags|trim|truncate:240|escapeHtml}{/if}" />
    <meta name="twitter:image" content="{$sArticle.image.source}" />
{/block}
