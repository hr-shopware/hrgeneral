{extends file="parent:frontend/detail/tabs/description.tpl"}

{* Product description title *}
{block name="frontend_detail_description_title"}
    <div class="content--title">
        {*s name="HrDetailDescriptionHeader"}Angebotsinformationen{/s*}Angebotsinformationen
    </div>
{/block}

{* Product description *}
{block name="frontend_detail_description_text"}
    <div class="product--description hr--custom-description" itemprop="description">
        {$sArticle.description_long|nl2br|boldHandler}<br>
    </div>
{/block}

{* Further links title *}
{block name="frontend_detail_description_links_title"}{/block}
