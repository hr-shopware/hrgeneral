{extends file="parent:frontend/detail/tabs/comment.tpl"}

{* Review title *}
{block name="frontend_detail_tabs_rating_title"}
    <h2>{s namespace="frontend/detail/comment" name="DetailCommentHeader"}{/s} "{$sArticle.articleName|escape}"</h2>
    <p>{s namespace="frontend/detail/comment" name="EchtheitBewertungen"}Kundenbewertungen sowie Produkt-Sternebewertungen dienen Kunden als Hilfe,
        um mehr über das Produkt zu erfahren und die Kaufentscheidung zu erleichtern.
        Die Sternebewertung (von 5) ist ein Durchschnitt aller Bewertungen.
        Ab dem 28.05.2022 werden bei uns Bewertungen von Kunden als verifiziert ausgewiesen,
        die Käufen zugeordnet werden können und nachweislich über <i>shop.health-rise.de</i> getätigt wurden.
        Diese werden vor der Veröffentlichung überprüft und entsprechend gekennzeichnet.
        <br><a href="{url controller=custom sCustom=64}" title="Echtheit der Bewertungen"> Weitere Informationen erhalten Sie hier.</a>{/s}
    </p>
{/block}


