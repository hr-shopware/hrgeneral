{extends file="parent:frontend/detail/content.tpl"}

{block name="frontend_detail_index_detail"}
    {if {config name="hrNoTabs"}}
        {include file="frontend/detail/notabs.tpl"}
    {else}
        {$smarty.block.parent}
    {/if}
{/block}