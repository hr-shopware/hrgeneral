{extends file="parent:frontend/detail/comment/entry.tpl"}

{* Author content *}
{block name='frontend_detail_comment_author_content'}
    <span class="content--field" itemprop="author" itemtype="https://schema.org/Person" itemscope="">
        <span itemprop="name">{if $vote.name}{$vote.name}{else}{s namespace="frontend/detail/comment" name="DetailCommentAnonymousName"}{/s}{/if}</span>
    </span>
{/block}

{* Headline *}
{block name='frontend_detail_comment_headline'}
    <h3 class="content--title" itemprop="name">
        {$vote.headline}
    </h3>
{/block}

{* Author label *}
{block name='frontend_detail_comment_author_label'}
    <span class="content--label comment--from">{s namespace="frontend/detail/comment" name="DetailCommentInfoFrom"}{/s}</span>
{/block}

{* Review publish date label *}
{block name='frontend_detail_comment_date_label'}
    <span class="content--label comment--at">{s namespace="frontend/detail/comment" name="DetailCommentInfoAt"}{/s}</span>
{/block}
