
{$productAttributes = $sArticle.attributes.core}
{$sections = ['application' => 'Anwendung', 'consumption' => 'Verzehrempfehlung', 'warnings' => 'Warnhinweise', 'ingredients' => 'Inhaltsstoffe']}

<div class="noTab_detail_container">
    <div class="noTab_detail_container_left" >
        {include file="frontend/detail/content/data_block.tpl" title="Produktbeschreibung" tpl="tabs/description.tpl"}
        {include file="frontend/detail/content/data_block.tpl" title="Bewertung" tpl="tabs/comment.tpl"}
        {if !empty($productAttributes['faq']|trim)}
            {include file="frontend/detail/content/data_block.tpl" title="FAQ" content=$productAttributes['faq']}
        {/if}
    </div>
    <div class="noTab_detail_container_right">
        {include file="frontend/detail/content/data_block.tpl" class="" title="Produktdetails" tpl="custom_tabs/productdetails.tpl"}
        {include file="frontend/detail/content/data_block.tpl" class="" title="So geht's" tpl="custom_tabs/howitswork.tpl"}
        {foreach $sections as $section => $sTitle}
            {if !empty($productAttributes[$section]|trim)}
                {include file="frontend/detail/content/data_block.tpl" class="" title=$sTitle content=$productAttributes[$section]}
            {/if}
        {/foreach}
    </div>
</div>