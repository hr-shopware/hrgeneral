{extends file="parent:frontend/detail/data.tpl"}


{* Unit price content *}
{block name='frontend_detail_data_price_unit_content'}
    {if $sArticle.purchaseunit < 1  && $sArticle.sUnit.description=="Liter"}
        {{$sArticle.purchaseunit}*1000} Milliliter
    {elseif $sArticle.purchaseunit < 1  && $sArticle.sUnit.description=="Kilogramm"}
        {{$sArticle.purchaseunit}*1000} Gramm
    {else}
        {$sArticle.purchaseunit} {$sArticle.sUnit.description}
    {/if}
{/block}

{* Reference unit price content *}
{block name='frontend_detail_data_price_unit_reference_content'}
    ({$sArticle.referenceprice|currency}
    / {if $sArticle.referenceunit!=1}{$sArticle.referenceunit}{/if} {$sArticle.sUnit.description})
{/block}


{*block name="frontend_detail_data_delivery"}{/block*}

{* Tax information *}
{*block name='frontend_detail_data_tax'}
    <p class="product--tax">
        {s name="DetailDataPriceInfo"}{/s}
    </p>
{/block*}


{* Regular price *}
{*block name='frontend_detail_data_price_default'}
    <span class="price--content content--default">
		<meta itemprop="price" content="{$sArticle.price|replace:',':'.'}" />
        {if $sArticle.priceStartingFrom}{s name='ListingBoxArticleStartsAt' namespace="frontend/listing/box_article"}{/s} {/if}{$sArticle.price|currency} {s name="Star" namespace="frontend/listing/box_article"}{/s}
    </span>

    <div class="is--hidden">
		<div itemprop="priceSpecification" itemscope itemtype="http://schema.org/PriceSpecification">
			<meta itemprop="price" content="{$sArticle.pseudoprice|replace:',':'.'}" />
			<meta itemprop="priceCurrency" content="EUR" />
		</div>

		<div itemprop="priceSpecification" itemscope itemtype="http://schema.org/PriceSpecification">
			<meta itemprop="price" content="{$sArticle.price|replace:',':'.'}"/>
			<meta itemprop="priceCurrency" content="EUR" />
			<meta itemprop="validThrough" content="2020-11-05" />
		</div>

        <meta itemprop="priceValidUntil" content="2020-11-05" />

		{if $sArticle.esd || $sArticle.instock >= $sArticle.minpurchase}
            <link itemprop="availability" href="http://schema.org/InStock" />
        {elseif $sArticle.sReleaseDate && $sArticle.sReleaseDate|date_format:"%Y%m%d" > $smarty.now|date_format:"%Y%m%d"}
            <link itemprop="availability" href="http://schema.org/PreOrder" />
        {else}
            <link itemprop="availability" href="http://schema.org/LimitedAvailability" />
        {/if}

		{foreach $sArticle.supplierBranches as $branch}
			{if $sArticle.attr10 != 1}
				<div itemscope itemtype="http://schema.org/HealthAndBeautyBusiness">
					<meta itemprop="name" content="{$branch.label}" />
					<meta itemprop="image" content="{$sArticle.supplierImg}" />
					<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
						<meta itemprop="streetAddress" content="{$branch.addressline1} {$branch.housenumber}" />
						<meta itemprop="addressLocality" content="{$branch.city}" />

						{if $branch.region != null}
							<meta itemprop="addressRegion" content="{$branch.region}" />"
						{/if}

						{if $branch.zipcode != null}
							<meta itemprop="postalCode" content="{$branch.zipcode}" />
						{/if}

						{if $branch.country == "D"}
							<meta itemprop="addressCountry" content="Deutschland" />
						{/if}

					</div>

					{if $branch.latitude != null && $branch.longitude != null}
						<div itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
							<meta itemprop="latitude" content="{$branch.latitude}"/>
							<meta itemprop="longitude" content="{$branch.longitude}"/>
						</div>
					{/if}

					{if $branch.phonenumber != null}
						<meta itemprop="telephone" content="{$branch.phonenumber}" />
					{/if}

					{if $sArticle.supplier_attributes.core->get('opening_time') != null}
						{assign var=openingTimes value=$sArticle.supplier_attributes.core->get('opening_time')|json_decode:1}
						{foreach $openingTimes as $openingTime}
							{if $openingTime['BranchId'] == $branch.id || $openingTime['BranchId'] == null && $branch.mainbranch == "1"}
								{assign var="weekDaysArray" value=[Mo,Tu,We,Th,Fr,Sa,Su]}
								<meta itemprop="openingHours" content="{$weekDaysArray[$openingTime['Day'] - 1]} {$openingTime['From']}-{$openingTime['To']}" />
							{/if}
						{/foreach}
					{/if}

					<meta itemprop="currenciesAccepted" content="EUR" />
					<meta itemprop="paymentAccepted" content="Kreditkarte, SOFORT Ueberweisung, Giropay, Lastschrift, PayPal" />
				</div>
			{/if}
		{/foreach}
	</div>
{/block*}
