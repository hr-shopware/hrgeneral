{extends file="parent:frontend/index/index.tpl"}

{block name="frontend_index_body_classes"}{strip}
    is--ctl-{controllerName|lower} is--act-{controllerAction|lower}
    {if $sUserLoggedIn} is--user{/if}
    {if $sOneTimeAccount} is--one-time-account{/if}
    {if $sTarget} is--target-{$sTarget|escapeHtml}{/if}
    {if $theme.checkoutHeader && (({controllerName|lower} == "checkout" && {controllerAction|lower} != "cart") || ({controllerName|lower} == "register" && ($sTarget != "account" && $sTarget != "address")))} is--minimal-header{/if}
    {if !$showSidebar && (!$theme.displaySidebar || $hasEmotion)} is--no-sidebar{/if}
{/strip}{/block}

{* Breadcrumb only if it is no emotion *}
{block name='frontend_index_breadcrumb'}
    {if $sBreadcrumb|count && !$hasEmotion}
        <nav class="content--breadcrumb block">
            {block name='frontend_index_breadcrumb_inner'}
                {include file='frontend/index/breadcrumb.tpl'}
            {/block}
        </nav>
    {/if}
{/block}

{block name='frontend_index_logo_trusted_shops'}
    <div id="myCustomTrustbadge"></div>
{/block}
