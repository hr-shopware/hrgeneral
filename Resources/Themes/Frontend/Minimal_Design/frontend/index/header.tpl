{extends file="parent:frontend/index/header.tpl"}

{* Meta-Tags *}
{block name='frontend_index_header_meta_tags'}
    {$smarty.block.parent}
    <meta name="facebook-domain-verification" content="83o9aaw9io1vpgzkl250rxdkdzjhgm" />
{/block}

{block name='frontend_index_header_meta_robots'}{if {controllerName|lower} eq 'search'}{s name='ListingMetaRobots'}{/s}{else}{s name='IndexMetaRobots'}{/s}{/if}{/block}

{* Added Title-Shortcuts via Smartyextension and backend config *}
{block name='frontend_index_header_title'}{strip}{if $sBreadcrumb}{foreach from=$sBreadcrumb|array_reverse item=breadcrumb name="title"}{if $smarty.foreach.title.index > 0 && count($sBreadcrumb)>2}{$breadcrumb.name|shortcuts}{else}{$breadcrumb.name}{/if} | {/foreach}{/if}{{config name="sShopname"}|escapeHtml}{/strip}{/block}
