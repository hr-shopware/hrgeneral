{extends file="parent:frontend/index/footer-navigation.tpl"}

{* Service hotline *}
{block name="frontend_index_footer_column_service_hotline"}
    <div class="footer--column column--hotline is--first block">
        <div class="column--headline">Unsere Zahlungsmethoden</div>

        <div class="column--content pmt">
            {foreach $sAllPayments as $payment}
                {if $payment.active}
                    <div class="payment-image-wrapper">
                        <img src="{link file="frontend/_public/src/img/{$payment.name}.png"}"
                             alt="{$payment.description}"
                             title="{$payment.description}"
                        />
                    </div>
                {/if}
            {/foreach}
        </div>
    </div>
{/block}


{block name="frontend_index_footer_column_newsletter_form"}
    <form class="newsletter--form" action="{url controller="newsletter"}" method="post">
        <input type="hidden" value="1" name="subscribeToNewsletter"/>
        <input type="hidden" value="de_DE" name="language"/>

        {block name="frontend_index_footer_column_newsletter_form_field_wrapper"}
            <div class="content">
                {block name="frontend_index_footer_column_newsletter_form_field"}
                    <input type="email" aria-label="{s name="IndexFooterNewsletterValue"}{/s}" name="newsletter"
                           class="newsletter--field" placeholder="{s name="IndexFooterNewsletterValue"}{/s}"/>
                    {if {config name="newsletterCaptcha"} !== "nocaptcha"}
                        <input type="hidden" name="redirect">
                    {/if}
                {/block}

                {block name="frontend_index_footer_column_newsletter_form_submit"}
                    <button type="submit" aria-label="{s name='IndexFooterNewsletterSubmit'}{/s}"
                            class="newsletter--button btn">
                        <i class="icon--mail"></i> <span
                                class="button--text">{s name='IndexFooterNewsletterSubmit'}{/s}</span>
                    </button>
                {/block}
            </div>
        {/block}

        {* Data protection information *}
        {block name="frontend_index_footer_column_newsletter_privacy"}
            {if {config name="ACTDPRTEXT"} || {config name="ACTDPRCHECK"}}
                {$hideCheckbox=false}

                {* If a captcha is active, the user has to accept the privacy statement on the newsletter page *}
                {if {config name="newsletterCaptcha"} !== "nocaptcha"}
                    {$hideCheckbox=true}
                {/if}

                {include file="frontend/_includes/privacy.tpl" hideCheckbox=$hideCheckbox}
            {/if}
        {/block}
    </form>
{/block}
