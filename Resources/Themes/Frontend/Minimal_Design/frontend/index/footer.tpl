{extends file="parent:frontend/index/footer.tpl"}

{* Copyright *}
{block name="frontend_index_shopware_footer_copyright"}{/block}

{* Logo *}
{block name="frontend_index_shopware_footer_logo"}{/block}

{* Vat info *}
{block name='frontend_index_footer_vatinfo'}
    <div class="footer--vat-info">
        <div class="info--headline">
            Health Rise GmbH
        </div>
        <p class="vat-info--text">
            Schaberweg 28a, D-61348 Bad Homburg v. d. Höhe
        </p>
    </div>
{/block}
