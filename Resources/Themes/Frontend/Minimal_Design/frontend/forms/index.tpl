{extends file='parent:frontend/forms/index.tpl'}

{* Forms headline *}
{block name='frontend_forms_index_headline'}{/block}

{* Forms Content *}
{block name='frontend_forms_index_content'}
    {if $sSupport.sElements}
        <div class="panel has--border is--rounded">
            <div class="panel panel--body is--wide is--rounded">
                {if $sSupport.sElements}
                    <h1 class="forms--title">{$sSupport.name}</h1>
                    <div class="forms--text">{$sSupport.text}</div>
                {elseif $sSupport.text2}
                    {include file="frontend/_includes/messages.tpl" type="success" content=$sSupport.text2}
                {/if}
            </div>
            <div class="panel--body">
                {block name='frontend_forms_index_form_elements'}
                    {include file="frontend/forms/form-elements.tpl"}
                {/block}
            </div>
        </div>
    {/if}
{/block}
