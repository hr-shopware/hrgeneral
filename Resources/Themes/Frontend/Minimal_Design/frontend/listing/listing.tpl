{extends file="parent:frontend/listing/listing.tpl"}

{* Sorting and changing layout *}
{block name="frontend_listing_top_actions"}
    {if $sCategoryCurrent !== 3 && empty($sCategoryContent.cmsHeadline) && empty($sCategoryContent.cmsText) }
        <h1>Produkte der Kategorie: "{$sCategoryContent.description}"</h1>
    {/if}
    {$smarty.block.parent}
{/block}

{block name="frontend_listing_emotions_emotion"}
    {if !$emotion.attribute.move_to_bottom}
        {include file="parent:frontend/_includes/emotion.tpl"}
    {/if}
{/block}

{block name="frontend_listing_listing_wrapper"}
    {$smarty.block.parent}
    {foreach $emotions as $emotion}
        {if $emotion.fullscreen == 1}
            {$fullscreen = true}
        {/if}
        {if $emotion.attribute.move_to_bottom}
            {include file="parent:frontend/_includes/emotion.tpl"}
        {/if}
    {/foreach}
{/block}