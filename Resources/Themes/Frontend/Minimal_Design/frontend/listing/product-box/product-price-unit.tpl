{extends file="parent:frontend/listing/product-box/product-price-unit.tpl"}

{block name='frontend_listing_box_article_unit_content'}
    <span class="is--nowrap">
        {if $sArticle.purchaseunit < 1  && $sArticle.sUnit.description=="Liter"}
            {{$sArticle.purchaseunit}*1000} Milliliter
        {elseif $sArticle.purchaseunit < 1  && $sArticle.sUnit.description=="Kilogramm"}
            {{$sArticle.purchaseunit}*1000} Gramm
        {else}
            {$sArticle.purchaseunit} {$sArticle.sUnit.description}
        {/if}
    </span>
{/block}

{block name='frontend_listing_box_article_unit_reference_content'}
    <span class="is--nowrap">
    ({$sArticle.referenceprice|currency}
    / {if $sArticle.referenceunit!=1}{$sArticle.referenceunit}{/if} {$sArticle.sUnit.description})
    </span>
{/block}
