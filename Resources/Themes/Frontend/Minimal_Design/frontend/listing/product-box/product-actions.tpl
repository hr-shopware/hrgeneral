{extends file="parent:frontend/listing/product-box/product-actions.tpl"}

{* Compare button *}
{block name='frontend_listing_box_article_actions_compare'}
    {if {config name="compareShow"}}
        <form action="{url controller='compare' action='add_article' articleID=$sArticle.articleID _seo=false}" method="post">
            <button type="submit"
                    title="{s name='ListingBoxLinkCompare'}{/s}"
                    aria-label="{s name='ListingBoxLinkCompare'}{/s}"
                    class="product--action action--compare"
                    data-product-compare-add="true">
                <i class="hr-icon-tab"></i> {s name="ListingBoxLinkCompare" namespace="frontend/listing/box_article"}{/s}
            </button>
        </form>
    {/if}
{/block}

{* Note button *}
{block name='frontend_listing_box_article_actions_save'}
    <form action="{url controller='note' action='add' ordernumber=$sArticle.ordernumber _seo=false}" method="post">
        {s name="DetailLinkNotepad" namespace="frontend/detail/actions" assign="snippetDetailLinkNotepad"}{/s}
        <button type="submit"
                title="{$snippetDetailLinkNotepad|escape}"
                aria-label="{$snippetDetailLinkNotepad|escape}"
                class="product--action action--note"
                data-ajaxUrl="{url controller='note' action='ajaxAdd' ordernumber=$sArticle.ordernumber _seo=false}"
                data-text="{s name="DetailNotepadMarked"}{/s}">
            <i class="hr-icon-heart"></i> <span class="action--text">{s name="DetailLinkNotepadShort" namespace="frontend/detail/actions"}{/s}</span>
        </button>
    </form>
{/block}
