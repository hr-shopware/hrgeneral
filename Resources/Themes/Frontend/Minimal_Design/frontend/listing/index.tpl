{extends file="parent:frontend/listing/index.tpl"}

{* Category headline *}
{block name="frontend_listing_index_text"}
    {if !$hasEmotion}
        {block name="frontend_listing_text"}{/block}
    {/if}
{/block}

{* Listing *}
{block name="frontend_listing_index_listing"}
    {$smarty.block.parent}
    {if $sCategoryContent.parentId > 3 && {config name=hrShowCMSTextInShoppingWorld}}
        {include file='frontend/listing/text.tpl'}
    {/if}
{/block}

{block name='frontend_index_content_left'}

    {block name='frontend_index_controller_url'}
        {* Controller url for the found products counter *}
        {$countCtrlUrl = "{url module="widgets" controller="listing" action="listingCount" params=$ajaxCountUrlParams fullPath}"}
    {/block}

    {include file='frontend/listing/sidebar.tpl'}

{/block}
