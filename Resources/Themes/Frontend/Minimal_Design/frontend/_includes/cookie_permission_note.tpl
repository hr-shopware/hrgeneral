{extends file="parent:frontend/_includes/cookie_permission_note.tpl"}

{block name="cookie_permission_accept_button_fixed"}
    {if {config name="cookie_note_mode"} == 1}
        {if {config name="cookie_show_button"}}
            <a href="#" class="cookie-permission--accept-button is--primary btn is--large is--center">
                {s namespace="frontend/cookiepermission/index" name="cookiePermission/acceptAll"}{/s}
            </a>
        {/if}

        <a href="#" class="cookie-permission--configure-button btn is--large is--center" data-openConsentManager="true">
            {s namespace="frontend/cookiepermission/index" name="cookiePermission/configure"}{/s}
        </a>
    {else}
        <a href="#" class="cookie-permission--accept-button btn is--primary is--large is--center">
            {s namespace="frontend/cookiepermission/index" name="cookiePermission/buttonText"}{/s}
        </a>
    {/if}
{/block}