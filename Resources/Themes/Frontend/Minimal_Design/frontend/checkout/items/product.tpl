{extends file="parent:frontend/checkout/items/product.tpl"}

{block name="frontend_checkout_cart_item_image_container_inner"}
    {$image = $sBasketItem.additional_details.image}
    {$desc = $sBasketItem.articlename|escape}
    {if $image.thumbnails[0]}
        <a href="{$detailLink}" title="{$sBasketItem.articlename|strip_tags|escape}" class="table--media-link"
            {if {config name=detailmodal} && {controllerAction|lower} === 'confirm'}
           data-modalbox="true"
           data-content="{url controller="detail" action="productQuickView" ordernumber="{$sBasketItem.ordernumber}" fullPath}"
           data-mode="ajax"
           data-width="750"
           data-sizing="content"
           data-title="{$sBasketItem.articlename|strip_tags|escape}"
           data-updateImages="true"
           {/if}>
            {if $image.description}
                {$desc = $image.description|escape}
            {/if}
            <img srcset="{$image.thumbnails[0].sourceSet}" alt="{$sBasketItem.articlename|escape}" title="{$sBasketItem.articlename|escape|truncate:160}" />
        </a>
    {else}
        <img src="{link file='frontend/_public/src/img/no-picture.jpg'}" alt="{$sBasketItem.articlename|escape}" title="{$sBasketItem.articlename|escape|truncate:160}" />
    {/if}
{/block}

{* Product SKU number *}
{block name="frontend_checkout_cart_item_details_sku"}
	<p class="content--sku content"></p>
{/block}

{* Product delivery information *}
{block name="frontend_checkout_cart_item_delivery_informations"}{/block}
