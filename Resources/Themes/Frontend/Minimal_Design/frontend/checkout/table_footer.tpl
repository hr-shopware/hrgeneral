{extends file="parent:frontend/checkout/table_footer.tpl"}


{* Checkout advantages *}
{block name="frontend_checkout_footer_benefits_list_entry_4"}{/block}

{* Supported dispatch services *}
{block name="frontend_checkout_footer_dispatch"}{/block}

{* Supported payment services *}
{block name="frontend_checkout_footer_text_payment"}
    <div class="column--content pmt">
        {foreach $sPayments as $payment}
            {if $payment.active}
                <div class="payment-image-wrapper">
                    <img src="{link file="frontend/_public/src/img/{$payment.name}.png"}"
                         alt="{$payment.description}"
                         title="{$payment.description}"
                    />
                </div>
            {/if}
        {/foreach}
    </div>
{/block}
