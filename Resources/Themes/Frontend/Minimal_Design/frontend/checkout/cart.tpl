{extends file='parent:frontend/checkout/cart.tpl'}

{* Infos for free article from Promotion Suite*}
{block name='frontend_checkout_cart_premium'}
    {s namespace="frontend/health_rise/checkout/cart" name="campaign_images"}
        <div style="margin-top:1.25rem;">
        <img id="promo-desc-img-small" src="{media path="media/image/aktion-gutschein-tee-mobil.jpg"}"
             alt="Gratisartikel Gutschein und Tee" title="Gratisartikel Gutschein und Tee">
        <img id="promo-desc-img-large" src="{media path="media/image/aktionen-gutschein-tee.jpg"}"
             alt="Gratisartikel Gutschein und Tee" title="Gratisartikel Gutschein und Tee">
        </div>{/s}
    {$smarty.block.parent}
{/block}
