{extends file="parent:frontend/register/index.tpl"}

{* Captcha *}
{block name='frontend_register_index_form_captcha'}
    {$smarty.block.parent}

    {* Newsletter settings *}
    {block name="frontend_account_index_newsletter_settings_headline"}
        <h2 class="panel--title is--underline">{s name="NewsletterRegisterHeadline" namespace="frontend/newsletter/index"}{/s}</h2>
    {/block}

    {block name="frontend_account_index_newsletter_settings_content"}
        <div class="panel--body is--wide">

            {* Newsletter checkbox *}
            <div class="register--password-description">
                <input type="checkbox" form="register--form" name="register[personal][newsletter]" id="newsletter" value="1" />

            {* Newsletter label *}
                <label for="newsletter">
                    {s name="AccountLabelWantNewsletter" namespace="frontend/account/index"}Ja, ich möchte den kostenlosen {$sShopname} Newsletter erhalten. Sie können sich jederzeit wieder abmelden!{/s}
                </label>
            </div>
        </div>
    {/block}


{/block}
