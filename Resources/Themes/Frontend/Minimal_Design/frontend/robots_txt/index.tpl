{extends file="parent:frontend/robots_txt/index.tpl"}

{block name="frontend_robots_txt_disallows"}
    {$robotsTxt->setDisallow('/compare')}
    {$robotsTxt->setDisallow('/checkout')}
    {$robotsTxt->setDisallow('/register')}
    {$robotsTxt->setDisallow('/account')}
    {$robotsTxt->setDisallow('/address')}
    {$robotsTxt->setDisallow('/note')}
    {$robotsTxt->setDisallow('/widgets')}
    {$robotsTxt->setDisallow('/listing')}
    {$robotsTxt->setDisallow('/ticket')}
    {$robotsTxt->setDisallow('/tracking')}

    {block name="frontend_robots_txt_disallows_output"}
        {foreach $robotsTxt->getDisallows() as $disallow}
            {$disallow}
        {/foreach}
    {/block}
{/block}