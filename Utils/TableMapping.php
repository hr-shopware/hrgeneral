<?php

namespace HrGeneral\Utils;

class TableMapping
{
    public const ARTICLE_ATTRIBUTES          = 's_articles_attributes';
    public const ARTICLE_SUPPLIER_ATTRIBUTES = 's_articles_supplier_attributes';
    public const ARTICLE_SUPPLIER_BRANCHES   = 's_articles_supplier_branch';
    public const CATEGORIES_ATTRIBUTES       = 's_categories_attributes';
    public const EMOTION_ATTRIBUTES          = 's_emotion_attributes';
    public const MEDIA_ATTRIBUTES            = 's_media_attributes';
    public const USER_ATTRIBUTES             = 's_user_attributes';
    public const ALL_ATTRIBUTE_TABLES = [
        self::ARTICLE_ATTRIBUTES,
        self::ARTICLE_SUPPLIER_ATTRIBUTES,
        self::ARTICLE_SUPPLIER_BRANCHES,
        self::CATEGORIES_ATTRIBUTES,
        self::EMOTION_ATTRIBUTES,
        self::MEDIA_ATTRIBUTES,
        self::USER_ATTRIBUTES
    ];
}
