<?php

namespace HrGeneral\Subscriber;

use Enlight\Event\SubscriberInterface;
use Enlight_Event_EventArgs;
use Enlight_Event_Exception;
use Enlight_Exception;
use sBasket;
use Shopware\Components\DependencyInjection\Container;
use Shopware_Controllers_Frontend_Checkout;
use Enlight_Controller_Request_Request;

use Zend_Db_Adapter_Exception;

class AddVoucherCodeSubscriber implements SubscriberInterface
{
    /**
     * @var Container
     */
    private Container $container;

    public function build(Container $container): void
    {
        $this->container = $container;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'Enlight_Controller_Action_PreDispatch_Frontend_Checkout'      => 'addVoucherCode',
            'Enlight_Controller_Action_PreDispatch_Frontend_PaymentPaypal' => 'addVoucherCode',
        ];
    }

    /**
     * @throws Enlight_Event_Exception
     * @throws Zend_Db_Adapter_Exception
     * @throws Enlight_Exception
     */
    public function addVoucherCode(Enlight_EVent_EventArgs $args): void
    {
        /** @var Shopware_Controllers_Frontend_Checkout $subject */
        $subject = $args->get('subject');

        /** @var Enlight_Controller_Request_Request $request */
        $request     = $subject->Request();
        $actionName  = $request->getActionName();
        $voucherCode = $_SESSION['Shopware']['sVoucher'];

        if (isset($voucherCode) && !empty($voucherCode) && in_array($actionName, ["express", "confirm", "cart"])) {
            $modules = $this->container->get('modules');
            /** @var sBasket $basket */
            $basket = $modules->Basket();
            $basket->sAddVoucher($voucherCode);
        }
    }
}
