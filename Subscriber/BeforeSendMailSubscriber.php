<?php

namespace HrGeneral\Subscriber;

use Enlight\Event\SubscriberInterface;
use Enlight_Components_Db_Adapter_Pdo_Mysql;
use Enlight_Event_EventArgs;
use Enlight_Event_Exception;
use Enlight_Exception;
use Shopware_Components_Document;
use Zend_Mime;
use Zend_Mime_Part;

class BeforeSendMailSubscriber implements SubscriberInterface
{
    /**
     * @var array
     */
    private array $config;

    /**
     * @var string
     */
    private string $documentDir;

    /**
     * @var Enlight_Components_Db_Adapter_Pdo_Mysql
     */
    private Enlight_Components_Db_Adapter_Pdo_Mysql $db;

    public function __construct(
        array $config,
        Enlight_Components_Db_Adapter_Pdo_Mysql $db,
        $documentDir
    ) {
        $this->config      = $config;
        $this->db          = $db;
        $this->documentDir = $documentDir;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'Shopware_Modules_Order_SendMail_BeforeSend' => 'beforeOrderMailSend',
        ];
    }

    /**
     * @throws Enlight_Event_Exception
     * @throws Enlight_Exception
     */
    public function beforeOrderMailSend(Enlight_Event_EventArgs $args): void
    {
        $this->attachBillToMail($args);
    }

    /**
     * @throws Enlight_Event_Exception
     * @throws Enlight_Exception
     */
    public function attachBillToMail(Enlight_Event_EventArgs $args): void
    {
        $mail        = $args->get('mail');
        $variables   = $args->get('variables');
        $orderNumber = $variables['ordernumber'];
        if ($this->config['autoCreateInvoice']) {
            $pdfFileContent = $this->createBill($orderNumber);
            $filePath = rtrim($this->documentDir, '/') . '/' . $pdfFileContent;
            if ($this->config['addInvoiceToMail']) {
                $mail->addAttachment($this->createAttachment($filePath));
                $args->set('mail', $mail);
            }
        }
    }

    /**
     * @param $orderNumber
     *
     * @return string
     * @throws Enlight_Exception
     * @throws Enlight_Event_Exception
     * @throws Enlight_Exception
     */
    private function createBill($orderNumber): string
    {
        $renderer = 'pdf'; // html / pdf
        $sql      = 'select id from s_order where ordernumber=? ';
        $orderId  = $this->db->fetchRow($sql, [$orderNumber])['id'];
        $document = Shopware_Components_Document::initDocument(
            $orderId,
            1,
            [
                'netto'                   => false,
                'bid'                     => null,
                'voucher'                 => null,
                'date'                    => '',
                'delivery_date'           => '',
                'shippingCostsAsPosition' => $this->config['addShippingCostAsPosition'],
                '_renderer'               => $renderer,
                '_preview'                => 0,
                '_previewForcePagebreak'  => false,
                '_previewSample'          => false,
                'docComment'              => '',
                'forceTaxCheck'           => '',
            ]
        );
        $document->render();
        $sql = 'select hash from s_order_documents where orderID=? ';

        return $this->db->fetchRow($sql, [$orderId])['hash'] . '.' . $renderer;
    }

    /**
     * @param $filePath
     *
     * @return Zend_Mime_Part
     */
    private function createAttachment($filePath): Zend_Mime_Part
    {
        $content                     = file_get_contents($filePath);
        $zendAttachment              = new Zend_Mime_Part($content);
        $zendAttachment->type        = 'application/pdf';
        $zendAttachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
        $zendAttachment->encoding    = Zend_Mime::ENCODING_BASE64;
        $zendAttachment->filename    = 'invoice.pdf';

        return $zendAttachment;
    }
}
