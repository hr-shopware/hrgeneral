<?php

namespace HrGeneral\Subscriber;

use Enlight\Event\SubscriberInterface;
use Enlight_Hook_HookArgs;
use JsonException;
use Shopware_Controllers_Widgets_Listing;

class ListingCountSubscriber implements SubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            'Shopware_Controllers_Widgets_Listing::listingCountAction::after' => 'onAfterListingCountAction',
        ];
    }

    /**
     * @param Enlight_Hook_HookArgs $args
     *
     * @throws JsonException
     */
    public function onAfterListingCountAction(Enlight_Hook_HookArgs $args): void
    {
        /** @var Shopware_Controllers_Widgets_Listing $subject */
        $subject = $args->getSubject();
        $sSearchTherm = $subject->Request()->getParam('sSearch');
        $view = $subject->View();
        $body = $view->getAssign();

        if ($sSearchTherm !== null) {
            $origin = empty($_SERVER['HTTP_ORIGIN']) ? '' : trim($_SERVER['HTTP_ORIGIN']);
            if (!empty($origin)) {
                preg_match(
                    '/http(s)?:\/\/(([^\/]*)\.)?(health-rise.de|localhost|172.27.172.246:8080)/',
                    $origin,
                    $matches
                );
                if (!empty($matches)) {
                    $subject->Front()->Plugins()->ViewRenderer()->setNoRender();
                    $subject->Response()->setHeader('Access-Control-Allow-Origin', $origin, true);
                    $subject->Response()->setHeader(
                        'Access-Control-Allow-Methods',
                        'GET, POST, PUT, DELETE, OPTIONS',
                        true
                    );
                    $subject->Response()->setHeader(
                        'Access-Control-Allow-Headers',
                        '*, Authorization, authorization',
                        true
                    );
                    $subject->Response()->setHeader('Access-Control-Allow-Credentials', 'true', true);
                    $subject->Response()->setHeader('Content-type', 'application/json', true);
                    echo json_encode($body, JSON_THROW_ON_ERROR);
                }
            }
        }
    }
}
