<?php

namespace HrGeneral\Subscriber;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Enlight\Event\SubscriberInterface;
use Enlight_Controller_ActionEventArgs;
use Shopware\Bundle\AttributeBundle\Service\CrudService;
use Shopware_Controllers_Frontend_Index;

class EmotionAttributeSubscriber implements SubscriberInterface
{
    private CrudService $crudService;
    private Connection $connection;

    public function __construct(CrudService $crudService, Connection $connection)
    {
        $this->crudService = $crudService;
        $this->connection = $connection;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'Enlight_Controller_Action_PostDispatchSecure_Frontend' => 'onPostDispatch',
        ];
    }

    public function xonPostDispatch(Enlight_Controller_ActionEventArgs $args): void
    {
        /** @var Shopware_Controllers_Frontend_Index $subject */
        $subject    = $args->getSubject();
        $view       = $subject->View();
        $emotions   = $view->getAssign('emotions');
        $attributes = $this->getEmotionAttributes($emotions[0]['id']);
        $view->assign('emotionAttributes', $attributes[0]);
        $view->assign("test", $emotions);
    }

    private function getEmotionAttributes($eId): array
    {
        $qb = $this->connection->createQueryBuilder();

        $qb->select(['*'])
           ->from('s_emotion_attributes', 'ea')
           ->where('ea.emotionID = :eId');
        $qb->setParameter('eId', $eId);

        try {
            return $qb->execute()->fetchAllAssociative();
        } catch (Exception | \Doctrine\DBAL\Driver\Exception $e) {
            return [];
        }
    }

    /**
     * Assign attribute-data to $emotions since sw dont to this atm.
     *
     * @param Enlight_Controller_ActionEventArgs $args
     *
     * @throws Exception
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public function onPostDispatch(Enlight_Controller_ActionEventArgs $args): void
    {
        $view = $args->getSubject()->View();
        $attributeValues = $this->getEmotionAttributeValues();
        $emotions = $view->getAssign('emotions');

        $sidebar = false;
        foreach ($emotions as &$emotion) {
            $emotion['attribute'] = $attributeValues[$emotion['id']];
            if(isset($emotion['attribute']['show_ekw_sidebar']) && !empty($emotion['attribute']['show_ekw_sidebar'])){
                $sidebar = true;
            }
        }
        unset($emotion);

        $view->assign('emotions', $emotions);
        $view->assign('showSidebar', $sidebar);
    }

    /**
     * @throws Exception
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    private function getEmotionAttributeValues(): array
    {
        $query = $this->connection->createQueryBuilder();
        $query->select(
            $this->getEmotionAttributeColumns()
        )->from('s_emotion_attributes');

        return $query->execute()->fetchAllAssociativeIndexed();
    }

    private function getEmotionAttributeColumns(): array
    {
        $attributes = $this->crudService->getList('s_emotion_attributes');

        $columns = ['emotionID'];
        foreach ($attributes as $attribute) {
            if ($attribute->isIdentifier()) {
                continue;
            }
            $columns[] = $attribute->getColumnName();
        }
        return $columns;
    }
}