<?php

namespace HrGeneral\Subscriber;

use Enlight\Event\SubscriberInterface;
use HrGeneral\Services\AccountingExportService;
use Shopware_Components_Cron_CronJob;

class CronJobSubscriber implements SubscriberInterface
{
    private AccountingExportService $accountingExportService;

    public function __construct(AccountingExportService $accountingExportService)
    {
        $this->accountingExportService = $accountingExportService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'Shopware_CronJob_AccountingExport' => 'onCronJobAccountingExport',
        ];
    }

    public function onCronJobAccountingExport(Shopware_Components_Cron_CronJob $job): string
    {
        if (!$this->accountingExportService->isReady()) {
            return "Not ready yet. Delete local file or wait until first of month.";
        }

        return $this->accountingExportService->accountingExport();
    }

}
