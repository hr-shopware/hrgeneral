<?php

namespace HrGeneral\Subscriber;

use Enlight\Event\SubscriberInterface;
use Enlight_Event_EventArgs;
use Shopware_Controllers_Frontend_Checkout;

class PromotionCartSubscriber implements SubscriberInterface
{
    private array $configs;

    public function __construct($configs)
    {
        $this->configs = $configs;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'Enlight_Controller_Action_PreDispatch_Frontend_Checkout' => 'onPostDispatchCheckout'
        ];
    }

    public function onPostDispatchCheckout(Enlight_Event_EventArgs $args): void
    {
        /** @var Shopware_Controllers_Frontend_Checkout $subject */
        $subject = $args->getSubject();
        $view = $subject->View();
        $view->assign('showCartPromotion', $this->configs['showCartPromotion']);

    }
}