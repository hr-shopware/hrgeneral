<?php

namespace HrGeneral\Subscriber;

use Enlight_Event_EventArgs;
use Enlight\Event\SubscriberInterface;
use Enlight_Hook_HookArgs;
use HrGeneral\Services\NewsletterPortal;
use Shopware_Components_Config;
use Shopware_Components_Modules;
use Shopware_Components_Snippet_Manager;
use Shopware_Controllers_Frontend_Account;
use Shopware_Controllers_Frontend_Newsletter;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NewsletterSubscriber implements SubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    /**
     * @var NewsletterPortal
     */
    private NewsletterPortal $newsletterPortal;

    private bool $newsletter;

    private string $email;

    /**
     * @var object|Shopware_Components_Snippet_Manager|null
     */
    private $snippet;

    private Shopware_Components_Modules $modules;

    /**
     * @var object|Shopware_Components_Config|null
     */
    private $config;

    /**
     * NewsletterSubscriber constructor.
     *
     * @param ContainerInterface          $container
     * @param NewsletterPortal            $newsletterPortal
     * @param Shopware_Components_Modules $modules
     */
    public function __construct(
        ContainerInterface $container,
        NewsletterPortal $newsletterPortal,
        Shopware_Components_Modules $modules
    ) {
        $this->container        = $container;
        $this->newsletterPortal = $newsletterPortal;
        $this->snippet          = $this->container->get('snippets');
        $this->config           = $this->container->get('config');
        $this->modules          = $modules;
        $this->newsletter       = false;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'Shopware_Controllers_Frontend_Newsletter::indexAction::replace'       => 'onNewNewsletterIndexAction',
            'Enlight_Controller_Action_PostDispatch_Frontend_Account'              => 'onFrontendPostDispatchAccount',
            'Shopware_Controllers_Frontend_Account::saveNewsletterAction::replace' => 'onSaveNewsletterAction',
            'sAdmin::sUpdateNewsletter::replace'                                   => 'onUpdateNewsletter',
            'Shopware_Controllers_Frontend_Register::saveRegisterAction::before'   => 'onSaveRegister',
            'Shopware_Modules_Admin_SaveRegister_Successful'                       => 'updateNewsletter',
        ];
    }

    /**
     * Changes form on newsletter page to work with portal
     *
     * @param Enlight_Hook_HookArgs $args
     *
     */
    public function onNewNewsletterIndexAction(Enlight_Hook_HookArgs $args): void
    {
        /** @var Shopware_Controllers_Frontend_Newsletter $subject */
        $subject = $args->getSubject();
        $view    = $subject->View();
        $request = $subject->Request();
        $data    = $request->getPost();
        $email   = $data['newsletter'];
        $view->assign('voteConfirmed', false);
        $view->assign('sStatus', ['isNewRegistration' => false]);

        $subscribeToNewsletter = $data['subscribeToNewsletter'];
        $isActive              = $subscribeToNewsletter === '1';

        if (empty($email)) {
            return;
        }

        if ($subscribeToNewsletter === '1') {
            $view->assign(
                'sStatus',
                [
                    'code'    => 3,
                    'message' => $this->snippet->getNamespace('frontend')->get('sMailConfirmation'),
                ]
            );
        }

        if ($subscribeToNewsletter === '-1') {
            $view->assign(
                'sStatus',
                [
                    'code'    => 5,
                    'message' => $this->snippet->getNamespace('frontend/account/internalMessages')
                                               ->get('NewsletterMailDeleted', 'Your mail address was deleted'),
                ]
            );
        }

        $result = $this->newsletterPortal->setNewsletterRegistrationPortal($email, $isActive);

        if (!$result) {
            $view->assign(
                'sStatus',
                [
                    'code'    => 5,
                    'message' => $this->snippet->getNamespace('frontend/error/index')->get('ErrorIndexTitle'),
                ]
            );
        }
    }

    /**
     * Sets newsletter checkbox on account page with request data from portal
     *
     * @param Enlight_Event_EventArgs $args
     *
     */
    public function onFrontendPostDispatchAccount(Enlight_Event_EventArgs $args): void
    {
        $subject = $args->get('subject');
        if (!$subject) {
            return;
        }
        $view  = $subject->View();
        $admin = $this->modules->Admin();
        if (!$admin) {
            return;
        }
        $email                                        = $admin->sGetUserMailById();
        $isSubscribedPortal                           = $this->newsletterPortal->hasSubscriptionPortal($email);
        $userData                                     = $view->getAssign('sUserData');
        $userData['additional']['user']['newsletter'] = $isSubscribedPortal;
        $view->assign('sUserData', $userData);
    }

    /**
     * Changes Newsletter checkbox on account page to work with portal
     *
     * @param Enlight_Hook_HookArgs $arguments
     *
     */
    public function onSaveNewsletterAction(Enlight_Hook_HookArgs $arguments): void
    {
        /** @var Shopware_Controllers_Frontend_Account $subject */
        $subject = $arguments->getSubject();
        if (!$subject) {
            return;
        }

        $view  = $subject->View();
        $front = $this->container->get('front');
        if (!$front) {
            return;
        }
        $request = $front->Request();
        if (!$request) {
            return;
        }

        if ($request->isPost()) {
            $status = (bool)$request->getPost('newsletter');
            $admin  = $this->modules->Admin();
            $email  = $admin->sGetUserMailById();
            $result = $this->newsletterPortal->setNewsletterRegistrationPortal($email, $status);
            if ($result) {
                $successMessage = $status ? 'newsletter' : 'deletenewsletter';
                if ($status && $this->config->optinnewsletter) {
                    $successMessage = 'optinnewsletter';
                }

                $view->assign('sSuccessAction', $successMessage);
                $session = $this->container->get('session');
                if (!$session) {
                    return;
                }
                $session->set('sNewsletter', $status);
            } else {
                $view->assign('sSuccessAction', 'errormessage');
            }
        }
        $subject->forward('index');
    }

    /**
     * Changes update newsletter to work with portal for checkbox on account and confirm page
     *
     * @param Enlight_Hook_HookArgs $arguments
     *
     * @return bool
     */
    public function onUpdateNewsletter(Enlight_Hook_HookArgs $arguments): bool
    {
        $status = $arguments->get('status');
        $email  = $arguments->get('email');

        return $this->newsletterPortal->setNewsletterRegistrationPortal($email, $status);
    }

    /**
     * Saves the status of the newsletter checkbox on registration
     *
     * @param Enlight_Hook_HookArgs $args
     */
    public function onSaveRegister(Enlight_Hook_HookArgs $args): void
    {
        $subject = $args->getSubject();
        $request = $subject->Request();
        $data    = $request->getPost();

        if ($data['register']['personal']['newsletter'] === '1') {
            $this->newsletter = true;
            $this->email      = $data['register']['personal']['email'];
        }
    }

    /**
     * Sends newsletter subscription to portal when checkbox was set on registration
     *
     * @param Enlight_Event_EventArgs $arguments
     */
    public function updateNewsletter(Enlight_Event_EventArgs $arguments): void
    {
        if ($this->newsletter) {
            $this->modules->Admin()->sUpdateNewsletter($this->newsletter, $this->email, true);
        }
    }
}
