<?php

namespace HrGeneral\Subscriber;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Exception;
use Enlight\Event\SubscriberInterface;
use Enlight_Event_EventArgs;
use Shopware_Controllers_Frontend_Index;

class ActivePaymentTypeSubscriber implements SubscriberInterface
{
    /**
     * @var Connection
     */
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'Enlight_Controller_Action_PostDispatch_Frontend_Index' => 'beforeShopLoaded',
            'Enlight_Controller_Action_PostDispatch_Frontend_Checkout' => 'beforeShopLoaded',
        ];
    }

    /**
     * @throws Exception|\Doctrine\DBAL\Exception
     */
    public function beforeShopLoaded(Enlight_Event_EventArgs $args): void
    {
        /** @var Shopware_Controllers_Frontend_Index $subject */
        $subject = $args->getSubject();

        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder->select('*')
                     ->from('s_core_paymentmeans', 'p');

        $payments = $queryBuilder->execute()->fetchAllAssociative();
        $view     = $subject->View();
        $view->assign('sAllPayments', $payments);
    }
}
