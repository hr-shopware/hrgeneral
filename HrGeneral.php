<?php

namespace HrGeneral;

use HrGeneral\Components\CompilerPass\AddTemplatePluginDirCompilerPass;
use HrGeneral\Utils\TableMapping;
use Shopware\Bundle\AttributeBundle\Service\CrudServiceInterface;
use Shopware\Bundle\AttributeBundle\Service\TableMappingInterface;
use Shopware\Bundle\AttributeBundle\Service\TypeMapping;
use Shopware\Bundle\AttributeBundle\Service\TypeMappingInterface;
use Shopware\Components\Plugin;
use Exception;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UpdateContext;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class HrGeneral extends Plugin
{
    private ?CrudServiceInterface $crudService = null;

    private ?TableMappingInterface $tableMapping = null;

    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);
        $container->addCompilerPass(new AddTemplatePluginDirCompilerPass());
    }

    /**
     * @param InstallContext $context
     *
     * @throws Exception
     */
    public function install(InstallContext $context): void
    {
        $this->runConfiguration();
    }

    /**
     * @param UpdateContext $context
     *
     * @throws Exception
     */
    public function update(UpdateContext $context): void
    {
        $currentVersion = $context->getCurrentVersion();
        $this->runConfiguration($currentVersion);
        parent::update($context);
    }

    private function getCrudService(): void
    {
        if (!$this->crudService) {
            /** @var CrudServiceInterface $crudService */
            $crudService       = $this->container->get('shopware_attribute.crud_service');
            $this->crudService = $crudService;
        }
    }

    private function getTableMapping(): void
    {
        if (!$this->tableMapping) {
            /** @var TableMappingInterface $tableMapping */
            $tableMapping       = $this->container->get('shopware_attribute.table_mapping');
            $this->tableMapping = $tableMapping;
        }
    }

    /**
     * @param string $table
     * @param array  $columns
     */
    private function deleteColumnsIfExists(string $table, array $columns = []): void
    {
        if (!empty($columns) && !empty($table)) {
            $this->getCrudService();
            $this->getTableMapping();

            foreach ($columns as $column) {
                if ($this->tableMapping->isTableColumn($table, $column)) {
                    $this->crudService->delete(
                        $table,
                        $column
                    );
                }
            }
        }
    }

    /**
     * @param string $existingVersion
     *
     * @throws Exception
     */
    private function runConfiguration(string $existingVersion = '1.0.0'): void
    {
        $this->getCrudService();

        if ($this->crudService) {
            switch (true) {
                case version_compare($existingVersion, '1.0.1', '<='):
                    $this->crudService->update(
                        TableMapping::EMOTION_ATTRIBUTES,
                        'meta_robots_index',
                        TypeMappingInterface::TYPE_BOOLEAN,
                        [
                            'displayInBackend' => true,
                            'label'            => 'Einkaufwelt - Meta Robots Index',
                            'supportText'      => 'Setzt die Einkaufswelt auf index/noindex',
                            'translatable'     => false,
                            'custom'           => false,
                        ],
                        null,
                        false,
                        1
                    ); //cmsHeader

                case version_compare($existingVersion, '1.0.23', '<='):
                    $this->deleteColumnsIfExists(TableMapping::EMOTION_ATTRIBUTES, ['meta_robots_index']);

                case version_compare($existingVersion, '2.0.13', '<='):
                    $this->crudService->update(
                        TableMapping::EMOTION_ATTRIBUTES,
                        'show_ekw_sidebar',
                        TypeMappingInterface::TYPE_BOOLEAN,
                        [
                            'custom'           => false,
                            'displayInBackend' => true,
                            'label'            => 'Sidebar in Einkaufswelt',
                            'supportText'      => 'Zeigt/Versteckt die Sidebar auf der Einkaufswelt',
                            'translatable'     => false,
                        ],
                        null,
                        false,
                        0
                    ); //cmsHeader

                case version_compare($existingVersion, '2.0.16', '<='):
                    $this->crudService->update(
                        TableMapping::EMOTION_ATTRIBUTES,
                        'move_to_bottom',
                        TypeMappingInterface::TYPE_BOOLEAN,
                        [
                            'custom'           => false,
                            'displayInBackend' => true,
                            'label'            => 'Einkaufswelt unter Listing',
                            'translatable'     => false,
                        ]
                    );

                case version_compare($existingVersion, '2.1.1', '<='):

                    $suffix = " (JTL)";
                    $fields = [
                        'ingredients' => 'Inhaltsstoffe',
                        'warnings'    => 'Warnhinweise',
                        'faq'         => 'FAQ',
                        'consumption' => 'Verzehrempfehlung',
                        'application' => 'Anwendung',
                    ];

                    foreach ($fields as $key => $value) {
                        $this->crudService->update(
                            TableMapping::ARTICLE_ATTRIBUTES,
                            $key,
                            TypeMappingInterface::TYPE_TEXT,
                            [
                                'custom'           => false,
                                'displayInBackend' => true,
                                'label'            => $value . $suffix,
                                'translatable'     => false,
                                'readonly'         => true,
                            ],
                            null,
                            true
                        );
                    }
            }
            $this->reGenerateAttributes();
        }
    }

    private function reGenerateAttributes(): void
    {
        $modelManager = $this->container->get('models');
        if ($modelManager !== null) {
            $modelManager->generateAttributeModels(TableMapping::ALL_ATTRIBUTE_TABLES);
        }
    }
}
